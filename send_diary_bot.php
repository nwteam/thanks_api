<?php
include('include/init.php');

$todays_date = date("Y-m-d");
$todays_week = date("w");// "0" (日曜日) ～ "6" (土曜日)
$todays_month = date("n");
$todays_day = date("j");
$now_hour = date("H");

//$member_id="999999999";
$stamp_id="1";
$img_url="";
error_log("Bot自動投稿処理開始！". date('Y/m/d H:i:s')."\r\n", 3, DOCUMENT_ROOT.'/log/diary_bot.log');
//Bot関係管理リスト取得(店舗+Bot 単位)
$bot_relation_list = get_bot_relation_list($now_hour);
if ($bot_relation_list) {
	foreach ($bot_relation_list as $bot_relation_list_key => $bot_relation_list_val) {
		$month_flg="0";//月配信FLG 0:配信しない 1:配信
		$week_flg="0";//週配信FLG 0:配信しない 1:配信
		$day_flg="0";//日配信FLG 0:配信しない 1:配信
		$diff="0";
//		$relation_id=$bot_relation_list_val['id'];//Bot関係管理ID
		$shop_id=$bot_relation_list_val['shop_id'];//店舗ID
		$bot_id=$bot_relation_list_val['bot_id'];//BOTID
		$member_id=$bot_relation_list_val['bot_member_id'];//BotユーザID
		$shop_info = get_shop_info($shop_id);
		$contents_id=$bot_relation_list_val['contents_id'];//コンテンツID
		$send_cycle_month_date=$bot_relation_list_val['send_cycle_month_date'];//毎月n日
		$send_cycle_week_day=$bot_relation_list_val['send_cycle_week_day'];//毎週n曜日
		$send_cycle_day_once=$bot_relation_list_val['send_cycle_day_once'];//n日に１回
		$send_method=$bot_relation_list_val['send_method'];//配信方法 0:順列 1:ランダム
		$send_loop_flg=$bot_relation_list_val['send_loop_flg'];//ループ 0:On 1:Off
		$execute_timeh=$bot_relation_list_val['execute_timeh'];//実施時間。24時間制2桁

		//最終配信コンテンツ情報取得
		$last_content_info = get_bot_last_content_info($shop_id,$bot_id);
		$last_send_contents_id=$last_content_info['last_send_contents_id'];//最終配信コンテンツID
		$last_send_time=$last_content_info['last_send_time'];//最終配信日時

		//配信頻度
		if($last_send_contents_id=="0"&&$send_cycle_day_once!="0"&&$send_cycle_month_date=="0"&&$send_cycle_week_day=="9"){
			$day_flg="1";
		}else{
			$last_send_day = date('Y-m-d', strtotime($last_send_time));
			$diff = diffBetweenTwoDays($last_send_day, $todays_date);
		}
		if($send_cycle_month_date==$todays_day){//毎月n日
			$month_flg="1";
		}
		if($send_cycle_week_day==$todays_week){//毎週n曜日
			$week_flg="1";
		}
		if($diff>0&&$diff>=$send_cycle_day_once){//n日に１回
			$day_flg="1";
		}

		if(($month_flg=="1"||$week_flg=="1"||$day_flg=="1")&&($shop_info['status'] == "0")&&($execute_timeh == $now_hour)){
			error_log("Bot情報：". date('Y/m/d H:i:s')." Shop_id:$shop_id BotID:$bot_id 配信方法(0:順列 1:ランダム):$send_method ループ(0:On 1:Off):$send_loop_flg\r\n", 3, DOCUMENT_ROOT.'/log/diary_bot.log');
			if($send_method=="0"){//配信方法 0:順列
				if($send_loop_flg=="0") {//ループ 0:On 1:Off
					$max_contents_id=get_bot_max_contents_id($shop_id,$bot_id);
					if($max_contents_id==$last_send_contents_id){
						init_bot_last_send_id($shop_id,$bot_id);//最終配信コンテンツ情報初期化
						$last_send_contents_id=0;
					}
				}
				$bot_contents_id = get_bot_contents_id($shop_id,$bot_id,$last_send_contents_id);
				$bot_content_info = get_bot_content_info($bot_contents_id);
				if ($bot_content_info) {
					$var = create_diary($member_id, $shop_id, $bot_content_info["stamp_id"], $bot_content_info["contents"], $bot_content_info["img_url"]);
					$diary_id = $var;
					update_bot_send_info($shop_id,$bot_id,$bot_contents_id);//最終配信情報更新
					//チームの皆にPush通知
					//							$exec_file=DOCUMENT_ROOT.VERSION_PATH.'common/thread_push_new_diary.php';
					//							exec("nohup php -c '' $exec_file $member_id $shop_id $diary_id  > /dev/null &");
					$member_array = get_shop_member_list($shop_id);
					if ($member_array) {
						foreach ($member_array as $key => $target_member_info) {
							if ($target_member_info['status'] == "2" && $shop_info['status'] == "0" && $target_member_info['diary_notice_flg'] == "0") {
								$notice_msg = "日記に新しい投稿がありました。皆でThanks!を贈りましょう！";
								$notice_msg = html_tag_chg(SBC_DBC(trim(urldecode($notice_msg)), 0));//Noticeメッセージ
								if ($target_member_info['member_id'] != $member_id) {
									save_diary_notice_info($diary_id, $target_member_info['member_id'], $notice_msg, $member_id, $shop_id);
									send_push_notice($target_member_info, $notice_msg, "1", $target_member_info['diary_notice_flg']);//お知らせ一覧に遷移
								}
							}
						}
					}
				}
			}elseif($send_method=="1"){//配信方法 1:ランダム
				$bot_content_random = get_bot_content_random($last_send_contents_id);
				if($bot_content_random) {
					$var = create_diary($member_id,$shop_id,$bot_content_random["stamp_id"],$bot_content_random["contents"],$bot_content_random["img_url"]);
					$diary_id=$var;
					update_bot_send_info($shop_id,$bot_id,$bot_content_random["contents_id"]);//最終配信情報更新
					//チームの皆にPush通知
//							$exec_file=DOCUMENT_ROOT.VERSION_PATH.'common/thread_push_new_diary.php';
//							exec("nohup php -c '' $exec_file $member_id $shop_id $diary_id  > /dev/null &");
					$member_array = get_shop_member_list($shop_id);
					if ($member_array) {
						foreach ($member_array as $key => $target_member_info) {
							if ($target_member_info['status'] == "2" && $shop_info['status'] == "0"&&$target_member_info['diary_notice_flg'] == "0") {
								$notice_msg="日記に新しい投稿がありました。皆でThanks!を贈りましょう！";
								$notice_msg= html_tag_chg(SBC_DBC(trim(urldecode($notice_msg)),0));//Noticeメッセージ
								if($target_member_info['member_id']!=$member_id){
									save_diary_notice_info($diary_id,$target_member_info['member_id'],$notice_msg,$member_id,$shop_id);
									send_push_notice($target_member_info,$notice_msg,"1",$target_member_info['diary_notice_flg']);//お知らせ一覧に遷移
								}
							}
						}
					}
				}
			}
		}
	}
}
error_log("Bot自動投稿処理終了！". date('Y/m/d H:i:s')."\r\n", 3, DOCUMENT_ROOT.'/log/diary_bot.log');
?>
