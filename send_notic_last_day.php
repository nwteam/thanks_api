<?php
include('include/init.php');

$todays_date = date("Y-m-d");
$todays_day = date("d");
$todays_month = date("n");
$todays_month_with_zero = date("m");
$todays_year = date("Y");
$first_day = date('Y-m-01', strtotime($todays_date));
$end_day= date('d', strtotime("$first_day +1 month -1 day"));

$list = get_all_member_list();
if ($list) {
	foreach ( $list as $key => $val ) {
		$startdate=strtotime($val['service_start_day']);
		$enddate=strtotime(date('Y-m-d',time()));
		$days=round(($enddate-$startdate)/3600/24) ;

		$insert_time=strtotime($val['insert_time']);
		$today=strtotime(date('Y-m-d 23:59:59',time()));
		$sign_days=round(($today-$insert_time)/3600/24) ;

		$thanks_msg="";
		$target_member_name=$val['member_name'];

		$shop_info = get_shop_info($val['shop_id']);
		if($shop_info['status']=="0") {
			//毎月末
			if (($todays_day==$end_day)) {
				if ($val['device_type'] == "1") {
					$thanks_msg = "今月もお疲れ様でした！お世話になったメンバーにthanks!を贈りましょう！";
					if ($val['birthday_notice_flg'] == "0"&&$val['device_id']!="") {
						$badge = get_unread_thanks_count($val['member_id'],$val['shop_id']);
//						$var = send_push_ios($thanks_msg, $val['device_id'], $badge);
						$var = send_push_ios_with_type($thanks_msg, $val['device_id'], $badge,"2");//メンバー一覧に遷移
					}
				} else {
					$thanks_msg = "今月もお疲れ様でした！お世話になったメンバーにthanks!を贈りましょう！";
					if ($val['birthday_notice_flg'] == "0"&&$val['device_id']!="") {
						$registatoin_ids = array($val['device_id']);
//						$thanks_msg = array("msg" => $thanks_msg);
//						$var = send_push_android($thanks_msg, $registatoin_ids);
						$var = send_push_android_with_type($thanks_msg, $registatoin_ids,"2");//メンバー一覧に遷移
					}
				}
			}
		}
	}
}
?>
