<?php

function rencode($error = 0,$msg='',$arr,$name = '')
{
	if($name == '')$name = 'list';
	$res['result']['error'] = "$error";
    if($error!=0)$name='';
	$res['result']['msg'] = $msg;
    if(empty($arr))$arr = '';
    if($error==0)$res['result'][$name] = $arr;
	die(encodes($res));
}

function check_sign($arr,$get_sign)
{
	$secret_key = 'thisisthanksapp';//キー
	
    ksort($arr);
    reset($arr);

    $sign = '';
    foreach ($arr AS $key=>$val)
    {
	    if ($key != 'sign' && $key != 'a' && $key != 'c')
	    {
	    	$sign .= "$key=$val&";
	    }
    }
    substr($sign, 0, -1) . $secret_key;
    $sign = substr($sign, 0, -1) . $secret_key;
    
    $temp_sign = strtolower(md5($sign));
    $get_sign = strtolower($get_sign);
    
	if($temp_sign == $get_sign)
	{
		return true;
	}else{
		rencode(1,'signが正しくありません。');
	}
	
}

function SBC_DBC($str,$args2) { //0:半角->全角；1，全角->半角
    $DBC = Array( '＆','＜' , '＞' , '＂' , '＇','｜');
  $SBC = Array('&','<', '>', '"', '\'','|');
 if($args2==0)
  return str_replace($SBC,$DBC,$str);  //半角->全角
 if($args2==1)
  return str_replace($DBC,$SBC,$str);  //全角->半角
 else
  return false;
}
function html_tag_chg($val){
	$val = str_replace("onchange","ｏｎｃｈａｎｇｅ", $val);
	$val = str_replace("onmouseover","ｏｎｍｏｕｓｅｏｖｅｒ", $val);
	$val = str_replace("onload","ｏｎｌｏａｄ", $val);
	$val = str_replace("onerror","ｏｎｅｒｒｏｒ", $val);
	$val = str_replace("javascript","ｊａｖａｓｃｒｉｐｔ", $val);
	$val = str_replace("document","ｄｏｃｕｍｅｎｔ", $val);
	$val = str_replace("onclick","ｏｎｃｌｉｃｋ", $val);
	$val = str_replace("alert","ａｌｅｒｔ", $val);
	$val = str_replace("script","ｓｃｒｉｐｔ", $val);

	return $val;
}
function check_parameter($arr)
{
	foreach($arr as $key => $r)
	{
		if($r == '')
		{
			rencode(1,'入力に不備があります。');
		}
	}
}
function curl($path)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, "$path");
    curl_setopt($curl, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-CN; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10 QQDownload/1.7');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $contents = curl_exec($curl);
    curl_close($curl);
    return $contents;
}
function sdate($str)
{
	return date("$str",time());
}
function encodes($arg, $force = true)
{
    static $_force;
    if (is_null($_force))
    {
        $_force = $force;
    }

    if ($_force && function_exists('json_encode'))
    {
        return json_encode($arg);
    }

    $returnValue = '';
    $c           = '';
    $i           = '';
    $l           = '';
    $s           = '';
    $v           = '';
    $numeric     = true;

    switch (gettype($arg))
    {
        case 'array':
            foreach ($arg AS $i => $v)
            {
                if (!is_numeric($i))
                {
                    $numeric = false;
                    break;
                }
            }

            if ($numeric)
            {
                foreach ($arg AS $i => $v)
                {
                    if (strlen($s) > 0)
                    {
                        $s .= ',';
                    }
                    $s .= encodes($arg[$i]);
                }

                $returnValue = '[' . $s . ']';
            }
            else
            {
                foreach ($arg AS $i => $v)
                {
                    if (strlen($s) > 0)
                    {
                        $s .= ',';
                    }
                    $s .= encodes($i) . ':' . encodes($arg[$i]);
                }

                $returnValue = '{' . $s . '}';
            }
            break;

        case 'object':
            foreach (get_object_vars($arg) AS $i => $v)
            {
                $v = encodes($v);

                if (strlen($s) > 0)
                {
                    $s .= ',';
                }
                $s .= encodes($i) . ':' . $v;
            }

            $returnValue = '{' . $s . '}';
            break;

        case 'integer':
        case 'double':
            $returnValue = is_numeric($arg) ? (string) $arg : 'null';
            break;

        case 'string':
            $returnValue = '' . json_encode($arg) . '';
            break;

        case 'boolean':
            $returnValue = $arg?'true':'false';
            break;

        default:
            $returnValue = 'null';
    }

    return $returnValue;
}
function get_limit_str($page,$page_number = 10)
{
    $page = ($page > 0) ? $page : 1;
    $start = ($page-1)*$page_number;
    return $limit = " limit $start,$page_number";
}
function cp_uniqid(){
    return md5(uniqid(rand(), true));
}
function filename(){
    foreach($_FILES as $file) {
        $name=explode('.', $file['name']);
        $ext=end($name);
        $name=$name[0];
    }
    $name = time();
    if(file_exists(FILE_PATH.$name.'.'.$ext)){
        $rand='-'.substr(cp_uniqid(),-5);
    }
    return $name.$rand;
}
function request_post($url = '', $param = '') {
    if (empty($url) || empty($param)) {
        return false;
    }

    $postUrl = $url;
    $curlPost = $param;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$postUrl);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}
function send_push_ios($message,$device_id,$badge){
    $fp='';
    $err='';
    $errstr='';
    $r_msg='0';
    // Put your device token here (without spaces):
//	$deviceToken = '9bcf827309450c7638759708bf22485948f35d88492971cf33c6450e4bc3f18e';

    // Put your alert message here:
//	$message = 'My first push notification!';

    ////////////////////////////////////////////////////////////////////////////////

    $ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert',IOS_PEM_FILE );
    stream_context_set_option($ctx, 'ssl', 'passphrase', IOS_PEM_PASS);

    // Open a connection to the APNS server
    $fp = stream_socket_client(
        PUSH_URL, $err,
        $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    if (!$fp){
        $msg=  date('Y-m-d H:i:s',time())." iOS Failed to connect: $err $errstr \r\n";
        error_log($msg,3,DOCUMENT_ROOT.'log/gen.log');
        $r_msg=='1';
    }

//    $msg=  'Connected to APNS' . PHP_EOL;
//    error_log($msg,3,DOCUMENT_ROOT.'log/gen.log');

    // Create the payload body
    if($badge!='0'){
        $body['aps'] = array(
            'content-available' => 1,
            'alert' => $message,
            'sound' => 'default',
            'badge' => intval($badge)
        );
    }else{
        $body['aps'] = array(
            'content-available' => 1,
            'alert' => $message,
            'sound' => 'default',
        );
    }

    // Encode the payload as JSON
    $payload = json_encode($body);

    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $device_id) . pack('n', strlen($payload)) . $payload;

    // Send it to the server
    $result = fwrite($fp, $msg, strlen($msg));

    if (!$result){
        $msg= date('Y-m-d H:i:s',time())." iOS Push Failed! Device_id = $device_id \r\n";
        error_log($msg,3,DOCUMENT_ROOT.'log/gen.log');
        $r_msg=='1';
    }
    else{
        $msg=  date('Y-m-d H:i:s',time())." iOS Push Success! Device_id = $device_id badge = $badge\r\n";
        error_log($msg,3,DOCUMENT_ROOT.'log/gen.log');
        $r_msg=='1';
    }
    // Close the connection to the server
    fclose($fp);

    return $r_msg;
}
function send_push_ios_with_type($message,$device_id,$badge,$type){
    $fp='';
    $err='';
    $errstr='';
    $r_msg='0';
    // Put your device token here (without spaces):
//	$deviceToken = '9bcf827309450c7638759708bf22485948f35d88492971cf33c6450e4bc3f18e';

    // Put your alert message here:
//	$message = 'My first push notification!';

    ////////////////////////////////////////////////////////////////////////////////

    $ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert',IOS_PEM_FILE );
    stream_context_set_option($ctx, 'ssl', 'passphrase', IOS_PEM_PASS);

    // Open a connection to the APNS server
    $fp = stream_socket_client(
        PUSH_URL, $err,
        $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    if (!$fp){
        $msg=  date('Y-m-d H:i:s',time())." iOS Failed to connect: $err $errstr \r\n";
        error_log($msg,3,DOCUMENT_ROOT.'log/gen.log');
        $r_msg=='1';
    }

//    $msg=  'Connected to APNS' . PHP_EOL;
//    error_log($msg,3,DOCUMENT_ROOT.'log/gen.log');

    // Create the payload body
    if($badge!='0'){
        $body['aps'] = array(
            'content-available' => 1,
            'alert' => $message,
            'sound' => 'default',
            'badge' => intval($badge)
        );
    }else{
        $body['aps'] = array(
            'content-available' => 1,
            'alert' => $message,
            'sound' => 'default',
        );
    }
    $body['type'] = $type;

    // Encode the payload as JSON
    $payload = json_encode($body);

    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $device_id) . pack('n', strlen($payload)) . $payload;

    // Send it to the server
    $result = fwrite($fp, $msg, strlen($msg));

    if (!$result){
        $msg= date('Y-m-d H:i:s',time())." iOS Push Failed! Device_id = $device_id \r\n";
        error_log($msg,3,DOCUMENT_ROOT.'log/gen.log');
        $r_msg=='1';
    }
    else{
        $msg=  date('Y-m-d H:i:s',time())." iOS Push Success! Device_id = $device_id badge = $badge\r\n";
        error_log($msg,3,DOCUMENT_ROOT.'log/gen.log');
        $r_msg=='1';
    }
    // Close the connection to the server
    fclose($fp);

    return $r_msg;
}
function send_push_android($message,$device_id) {

    // Set POST variables
    $url = 'https://android.googleapis.com/gcm/send';
    $fields = array(
        'registration_ids' => $device_id,
        'data' => $message,
    );
    $headers = array(
        'Authorization: key=' . GOOGLE_API_KEY,
        'Content-Type: application/json'
    );
    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
//    error_log("Android Push result:$result \r\n",3,DOCUMENT_ROOT.'log/gen.log');
    if ($result === FALSE) {
//		die('Curl failed: ' . curl_error($ch));
        $result=1;
        error_log(date('Y-m-d H:i:s',time())." Android Push Failed! result=$result \r\n",3,DOCUMENT_ROOT.'log/gen.log');
        return $result;
    }else{
        error_log(date('Y-m-d H:i:s',time())." Android Push Success! result=$result\r\n",3,DOCUMENT_ROOT.'log/gen.log');
    }

    // Close connection
    curl_close($ch);
    $result=0;
    return $result;
}
function send_push_android_with_type($message,$device_id,$type) {

    // Set POST variables
    $url = 'https://android.googleapis.com/gcm/send';
    $fields = array(
        'registration_ids' => $device_id,
        'data' => array('msg'=>$message,'type'=>$type,),
    );
    $headers = array(
        'Authorization: key=' . GOOGLE_API_KEY,
        'Content-Type: application/json'
    );
    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    error_log("Fields:".json_encode($fields)." \r\n",3,DOCUMENT_ROOT.'log/gen.log');
    // Execute post
    $result = curl_exec($ch);
//    error_log("Android Push result:$result \r\n",3,DOCUMENT_ROOT.'log/gen.log');
    if ($result === FALSE) {
//		die('Curl failed: ' . curl_error($ch));
        $result=1;
        error_log(date('Y-m-d H:i:s',time())." Android Push Failed! result=$result \r\n",3,DOCUMENT_ROOT.'log/gen.log');
        return $result;
    }else{
        error_log(date('Y-m-d H:i:s',time())." Android Push Success! result=$result\r\n",3,DOCUMENT_ROOT.'log/gen.log');
    }

    // Close connection
    curl_close($ch);
    $result=0;
    return $result;
}
// 画像の方向を正す
function orientationFixedImage($output,$input){
    $image = ImageCreateFromJPEG($input);
    $exif_datas = @exif_read_data($input);
    if(isset($exif_datas['Orientation'])){
        $orientation = $exif_datas['Orientation'];
        if($image){
            // 未定義
            if($orientation == 0){
                // 通常
            }else if($orientation == 1){
                // 左右反転
            }else if($orientation == 2){
                image_flop($image);
                // 180°回転
            }else if($orientation == 3){
                image_rotate($image,180, 0);
                // 上下反転
            }else if($orientation == 4){
                image_Flip($image);
                // 反時計回りに90°回転 上下反転
            }else if($orientation == 5){
                image_rotate($image,270, 0);
                image_flip($image);
                // 時計回りに90°回転
            }else if($orientation == 6){
                image_rotate($image,90, 0);
                // 時計回りに90°回転 上下反転
            }else if($orientation == 7){
                image_rotate($image,90, 0);
                image_flip($image);
                // 反時計回りに90°回転
            }else if($orientation == 8){
                image_rotate($image,270, 0);
            }
        }
    }
    // 画像の書き出し
    ImageJPEG($image ,$output);
    return false;
}
// 画像の左右反転
function image_flop($image){
    // 画像の幅を取得
    $w = imagesx($image);
    // 画像の高さを取得
    $h = imagesy($image);
    // 変換後の画像の生成（元の画像と同じサイズ）
    $destImage = @imagecreatetruecolor($w,$h);
    // 逆側から色を取得
    for($i=($w-1);$i>=0;$i--){
        for($j=0;$j<$h;$j++){
            $color_index = imagecolorat($image,$i,$j);
            $colors = imagecolorsforindex($image,$color_index);
            imagesetpixel($destImage,abs($i-$w+1),$j,imagecolorallocate($destImage,$colors["red"],$colors["green"],$colors["blue"]));
        }
    }
    return $destImage;
}
// 上下反転
function image_flip($image){
    // 画像の幅を取得
    $w = imagesx($image);
    // 画像の高さを取得
    $h = imagesy($image);
    // 変換後の画像の生成（元の画像と同じサイズ）
    $destImage = @imagecreatetruecolor($w,$h);
    // 逆側から色を取得
    for($i=0;$i<$w;$i++){
        for($j=($h-1);$j>=0;$j--){
            $color_index = imagecolorat($image,$i,$j);
            $colors = imagecolorsforindex($image,$color_index);
            imagesetpixel($destImage,$i,abs($j-$h+1),imagecolorallocate($destImage,$colors["red"],$colors["green"],$colors["blue"]));
        }
    }
    return $destImage;
}
// 画像を回転
function image_rotate($image, $angle, $bgd_color){
    return imagerotate($image, $angle, $bgd_color, 0);
}
function diffBetweenTwoDays ($day1, $day2)
{
    $second1 = strtotime($day1);
    $second2 = strtotime($day2);

    if ($second1 < $second2) {
        $tmp = $second2;
        $second2 = $second1;
        $second1 = $tmp;
    }
    return ($second1 - $second2) / 86400;
}
?>
