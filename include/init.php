<?php
define('IN_ECS', true);
error_reporting(E_ALL);
date_default_timezone_set('Asia/Tokyo');
header("Content-type: text/html; charset=utf-8");
require('config.php');
require('common.php');
require('global.fun.php');
require('filter.php');
require('UploadFile.class.php');


extract($_REQUEST);

if (PHP_VERSION >= '5.1' && !empty($timezone))
{
    date_default_timezone_set($timezone);
}

require('cls_mysql.php');
$db = new cls_mysql($db_host, $db_user, $db_pass, $db_name);

//https://github.com/joshcam/PHP-MySQLi-Database-Class#initialization
require_once('MySQLi_DB_Class/MysqliDb.php');
//$db2 = new MysqliDb($db_host, $db_user, $db_pass, $db_name);
$db2 = new MysqliDb (Array (
                'host' => $db_host,
                'username' => $db_user,
                'password' => $db_pass,
                'db'=> $db_name,
                'charset' => 'utf8mb4'));
?>
