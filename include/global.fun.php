<?php
/***************************Memebers用 Start****************************/
//登録状態を検知するAPI
function get_member_status($member_id)
{
	global $db;

	return $db->getAll("select status,shop_id,thanks_notice_flg,birthday_notice_flg,diary_notice_flg from trn_members where member_id='$member_id'");
}
function check_email($email)
{
	global $db;

	return $db->getOne("select email from trn_members where email='$email' and status<>9 ");
}
function get_member_info_by_email($email)
{
	global $db;

	return $db->getRow("select * from trn_members where email='$email' and status<>9 ");
}
function reg_mail($email,$password)
{
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$time = date('Y-m-d H:i:s',time());
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$mysqli->query("SET character_set_results = 'utf8mb4', character_set_client = 'utf8mb4', character_set_connection = 'utf8mb4', character_set_database = 'utf8mb4', character_set_server = 'utf8mb4'");

	if (!($stmt = $mysqli->prepare("INSERT INTO trn_members (email,password,insert_time,update_time) VALUES (?,?,?,?)"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}

	if (!$stmt->bind_param("ssss", $email,$password,$time,$time)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}

	if (!$stmt->execute()) {
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$mysqli_return=mysqli_insert_id($mysqli);
	$mysqli->close();
	return $mysqli_return;
}
function reg_profile($member_id,$member_name,$birthday,$service_start_day,$self_introduction,$interest,$image_url)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());
//	$member_name= mysqli_real_escape_string($member_name);
//	$birthday= mysqli_real_escape_string($birthday);
//	$service_start_day= mysqli_real_escape_string($service_start_day);
//	$self_introduction= mysqli_real_escape_string($self_introduction);
//	$interest= mysqli_real_escape_string($interest);

	$arr = array('member_name'=>$member_name,'birthday'=>$birthday,'service_start_day'=>$service_start_day,'self_introduction'=>$self_introduction,'interest'=>$interest,'profile_img_url'=>$image_url,'status'=>1,'update_time'=>$time);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= '$member_id' and status=0");
}
function reg_shop($member_id,$shop_id,$device_type,$device_id)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('shop_id'=>$shop_id,'status'=>2,'final_login_time'=>$time,'update_time'=>$time,'device_type'=>$device_type,'device_id'=>$device_id);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= '$member_id' and status=1");
}
function reg_device($member_id,$device_type,$device_id)
{
	global $db;

	$arr = array('device_type'=>$device_type,'device_id'=>$device_id);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= '$member_id' and status=1");
}
function check_shop_auth_code($shop_auth_code)
{
	global $db;

	return $db->getRow("select * from mst_admin_user where shop_AuthCode='$shop_auth_code'");
}
function get_login_info($email)
{
	global $db;

	return $db->getRow("select * from trn_members where email='$email' and status>='2' order by status asc");
}
function get_member_info($member_id)
{
	global $db;

	return $db->getRow("select a.*,(select count(*) from trn_thanks b where b.target_id=a.member_id) as receive_thanks_cnt,(select count(*) from trn_thanks b where b.from_id=a.member_id) as send_thanks_cnt from trn_members a where a.member_id='$member_id'");
}
function get_shop_member_info($member_id,$shop_id)
{
	global $db;

	return $db->getRow("select tm.member_id,tm.email,tm.password,tt.shop_id,tm.member_name,tm.profile_img_url, tm.birthday,tm.service_start_day,tm.self_introduction,tm.interest,tm.device_type,tm.device_id, tm.thanks_notice_flg,tm.birthday_notice_flg,tm.auth_code,IF(tt.del_flg=0,tm.status,9) status,tm.bot_flg,tm.final_login_time, tm.insert_time,tm.update_time, (select IFNULL(sum(thanks_receives),0) from trn_thanks_cnt b where b.menber_id=tm.member_id) as receive_thanks_cnt,(select IFNULL(sum(thanks_sends),0) from trn_thanks_cnt b where b.menber_id=tm.member_id) as send_thanks_cnt from trn_members tm INNER JOIN trn_team tt ON tm.member_id=tt.member_id AND tt.shop_id='$shop_id' where tm.member_id='$member_id'");
}
function get_shop_info($shop_id)
{
	global $db;

	return $db->getRow("select a.* from mst_admin_user a where a.id='$shop_id'");
}
function edit_profile($member_id,$member_name,$birthday,$service_start_day,$self_introduction,$interest,$image_url)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());
//	$member_name= mysqli_real_escape_string($member_name);
//	$birthday= mysqli_real_escape_string($birthday);
//	$service_start_day= mysqli_real_escape_string($service_start_day);
//	$self_introduction= mysqli_real_escape_string($self_introduction);
//	$interest= mysqli_real_escape_string($interest);

	$arr = array('member_name'=>$member_name,'birthday'=>$birthday,'service_start_day'=>$service_start_day,'self_introduction'=>$self_introduction,'interest'=>$interest,'profile_img_url'=>$image_url,'update_time'=>$time);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= '$member_id' and status=2");
}
function edit_email($member_id,$email)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());
//	$email= mysqli_real_escape_string($email);

	$arr = array('email'=>$email,'update_time'=>$time);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= '$member_id' and status=2");
}
function edit_pw($member_id,$password)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());
//	$password= mysqli_real_escape_string($password);

	$arr = array('password'=>$password,'update_time'=>$time);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= '$member_id' and status=2");
}
function delete_account($member_id)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('status'=>9,'update_time'=>$time);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= '$member_id'");
}
function device_init($member_id)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('device_id'=>"",'update_time'=>$time);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= '$member_id'");
}
function set_thanks_notice_flg($member_id,$flg)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('thanks_notice_flg'=>$flg,'update_time'=>$time);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= '$member_id' and status=2");
}
function set_batch_notice_flg($member_id,$flg)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('birthday_notice_flg'=>$flg,'update_time'=>$time);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= '$member_id' and status=2");
}
function set_new_diary_notice_flg($member_id,$flg)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('diary_notice_flg'=>$flg,'update_time'=>$time);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= '$member_id' and status=2");
}
function upt_final_login($member_id)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('final_login_time'=>$time);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= '$member_id' and status=2");
}
function update_device_info($member_id,$device_type,$device_id)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('device_type'=>$device_type,'device_id'=>$device_id,'final_login_time'=>$time,'update_time'=>$time);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= '$member_id' and status=2");
}
/***************************Memebers用 End****************************/
/***************************Thanks用 Start****************************/
function get_receive_thanks_list($member_id,$shop_id,$page,$page_number)
{
	global $db;

	$limit = get_limit_str($page,$page_number);

	return $db->getAll("select a.*,IF(tm.status=2 AND tt.del_flg=0,tm.member_name,'退会済み') as member_name,IF(tm.status=2 AND tt.del_flg=0,tm.profile_img_url,'') as profile_img_url,tm.status as from_member_status ,(select image_url from mst_thanks_stamp as c where c.stamp_id=a.stamp_id) as stamp_image_url from trn_thanks as a LEFT JOIN trn_members as tm ON a.from_id=tm.member_id LEFT JOIN trn_team tt ON a.from_id=tt.member_id AND a.shop_id=tt.shop_id where 1 and a.from_id=tm.member_id and a.target_id = $member_id and a.shop_id= $shop_id order by thanks_id desc $limit");
}
function get_receive_unread_thanks_list($member_id,$shop_id)
{
	global $db;

	$limit = " LIMIT 0,".PAGE_MAX;

	return $db->getAll("select a.*,IF(tm.status=2 AND tt.del_flg=0,tm.member_name,'退会済み') as member_name,IF(tm.status=2 AND tt.del_flg=0,tm.profile_img_url,'') as profile_img_url,tm.status as from_member_status ,(select image_url from mst_thanks_stamp as c where c.stamp_id=a.stamp_id) as stamp_image_url from trn_thanks as a LEFT JOIN trn_members as tm ON a.from_id=tm.member_id LEFT JOIN trn_team tt ON a.from_id=tt.member_id AND a.shop_id=tt.shop_id where 1 and a.from_id=tm.member_id and a.target_id = $member_id and a.shop_id= $shop_id and a.status=0 order by thanks_id desc $limit");
}
function update_read_flg($thanks_id)
{
	global $db;

	$time = date('Y-m-d H:i:s',time());

	$arr = array('status'=>'1','read_time'=>$time);

	return $db->autoExecute('trn_thanks',$arr,'UPDATE'," thanks_id= $thanks_id and status=0");
}
function update_all_thanks_read_flg($member_id,$shop_id)
{
	global $db;

	$time = date('Y-m-d H:i:s',time());

	$arr = array('status'=>'1','read_time'=>$time);

	return $db->autoExecute('trn_thanks',$arr,'UPDATE'," target_id= $member_id and shop_id= $shop_id and status=0");
}
function get_receive_thanks_total_count($member_id,$shop_id)
{
	global $db;

//	return $db->getOne("select count(*) from trn_thanks where target_id = $member_id ");
//	多チーム切替対応
	return $db->getOne("select count(*) from trn_thanks where target_id = $member_id and shop_id=$shop_id");
}
function get_send_thanks_list($member_id,$shop_id,$page,$page_number)
{
	global $db;

	$limit = get_limit_str($page,$page_number);

	return $db->getAll("select a.*,IF(tm.status=2 AND tt.del_flg=0,tm.member_name,'退会済み') as member_name,IF(tm.status=2 AND tt.del_flg=0,tm.profile_img_url,'') as profile_img_url,tm.status as from_member_status,(select image_url from mst_thanks_stamp as c where c.stamp_id=a.stamp_id) as stamp_image_url from trn_thanks as a LEFT JOIN trn_members as tm ON a.target_id=tm.member_id LEFT JOIN trn_team tt ON a.target_id=tt.member_id AND a.shop_id=tt.shop_id where a.status<>2 and a.from_id =$member_id and a.shop_id=$shop_id order by thanks_id desc $limit");
}
function get_send_thanks_total_count($member_id,$shop_id)
{
	global $db;

	return $db->getOne("select count(*) from trn_thanks where from_id = $member_id and shop_id=$shop_id");
}
function get_unread_thanks_count($member_id,$shop_id)
{
	global $db;

#	return $db->getOne("select count(*) from trn_thanks where target_id = $member_id and status=0 ");
#多チーム切替対応
	return $db->getOne("select count(*) from trn_thanks where target_id =$member_id and shop_id=$shop_id and status=0 ");

}
function get_unread_count($member_id,$shop_id)
{
	$unread_thanks_count=get_unread_thanks_count($member_id,$shop_id);
	$unread_notice_count=get_unread_notice_count($member_id,$shop_id);

	$unread_count=$unread_thanks_count+$unread_notice_count;

	return $unread_count;
}
//#廃止
//#function get_service_100days_staff_list($shop_id)
//#{
//#	global $db;
//#
//#	return $db->getAll("select *,'100days_member' from trn_members where service_start_day BETWEEN DATE_ADD(service_start_day, INTERVAL 100 DAY) AND DATE_ADD(service_start_day, INTERVAL 107 DAY) and status=2 and shop_id = $shop_id order by final_login_time desc ");
//#	return $db->getAll("select *,'100days_member' from trn_members where service_start_day BETWEEN DATE_ADD(service_start_day, INTERVAL 100 DAY) AND DATE_ADD(service_start_day, INTERVAL 107 DAY) and status=2 and shop_id = $shop_id order by member_name ");
//}
//#廃止
//#function get_birthday_staff_list($shop_id)
//#{
//#	global $db;
//#
//#	return $db->getAll("select *,'birthday_member' from trn_members where month(birthday)=month(now()) and status=2 and shop_id = $shop_id order by final_login_time desc ");
//#	return $db->getAll("select *,'birthday_member' from trn_members where month(birthday)=month(now()) and status=2 and shop_id = $shop_id order by member_name ");
//#}
function get_nomal_staff_list($shop_id,$member_id)
{
	global $db;

#	return $db->getAll("select * from trn_members where status=2 and shop_id = $shop_id and member_id <> $member_id order by final_login_time desc ");
#	return $db->getAll("select * from trn_members where status=2 and shop_id = $shop_id and member_id <> $member_id order by member_name ");
#多チーム切替対応
	return $db->getAll("select tm.* from trn_members tm INNER JOIN trn_team tt ON tm.member_id=tt.member_id and tt.del_flg=0  where tm.status=2 and tt.shop_id = $shop_id and tm.member_id <> $member_id order by tm.member_name ");
}
function get_all_member_list()
{
	global $db;

#	return $db->getAll("select * from trn_members where status=2 and birthday_notice_flg = 0 order by final_login_time desc ");
#	return $db->getAll("select * from trn_members where status=2 and birthday_notice_flg = 0 order by member_name ");
#多チーム切替対応
	return $db->getAll("select tm.device_id,tm.birthday_notice_flg,tm.device_type,tm.status,tm.member_id,tt.shop_id,tm.member_name,tm.birthday,tm.service_start_day,tm.insert_time from trn_members tm INNER JOIN trn_team tt ON tm.member_id=tt.member_id and tt.del_flg=0 where tm.status=2 and tm.birthday_notice_flg = 0 order by member_name ");

}
function get_all_shop_list()
{
	global $db;
	return $db->getAll("select shop_id from trn_team where del_flg=0 GROUP by shop_id ");

}
function get_shop_member_list($shop_id)
{
	global $db;

#	return $db->getAll("select * from trn_members where status=2 and birthday_notice_flg = 0 and shop_id = $shop_id order by final_login_time desc ");
#	return $db->getAll("select * from trn_members where status=2 and birthday_notice_flg = 0 and shop_id = $shop_id order by member_name ");
#多チーム切替対応
	return $db->getAll("select tm.* from trn_members tm INNER JOIN trn_team tt ON tm.member_id=tt.member_id AND tt.del_flg=0 where tm.status=2 and tt.shop_id =$shop_id order by member_name ");
}
function send_thanks($target_id,$stamp_id,$thanks_msg,$member_id,$shop_id,$point_status,$point_charge_status,$point_thanks_send,$point_thanks_receive)
{
	global $db2;
	$time = date('Y-m-d H:i:s',time());
    $db2->startTransaction();
    //set thanks
    $data = Array ("target_id" => $target_id,
               "stamp_id" => $stamp_id,
               "thanks_msg" => $thanks_msg,
               "from_id" => $member_id,
               "send_time" => $time,
               "shop_id" => $shop_id
    );
	$id = $db2->insert ('trn_thanks', $data);
	if($id){
		$check_cnt=get_point_trade_before_count($member_id,$target_id,$shop_id,"1");
		$check_cnt2=get_point_trade_before_count($member_id,$target_id,$shop_id,"2");
		if($point_status=="0"&&$point_charge_status=="0"&&$target_id!=$member_id&&$check_cnt=="0"&&$check_cnt2=="0"){
			if($point_thanks_send!="0"){
				//ポイント獲得(送信者にポイント付与)
				$var=add_point($db2,$member_id,$shop_id,'1',$id,$point_thanks_send);
			}else{
				$var=$id;
			}
			if($var){
				if($point_thanks_receive!="0"){
					//ポイント獲得(受信者にポイント付与)
					$var=add_point($db2,$target_id,$shop_id,'2',$id,$point_thanks_receive);
				}
				if($var){
					$db2->commit();
				}else{
					$db2->rollback();
					$id=null;
				}
			}else{
				$db2->rollback();
				$id=null;
			}
		}else{
			 $db2->commit();
		}
	}else{
		$db2->rollback();
		$id=null;
	}
	return $id;
}
//function get_thanks_stamp_list()
//{
//	global $db;
//
//	return $db->getAll("select * from mst_thanks_stamp where status=0 order by sort asc ");
//}
function get_thanks_stamp_list($category_id,$member_id,$shop_id,$agency_id,$chain_id)
{
	global $db;
	return $db->getAll("
		SELECT c.* FROM (
		/*　全体共通STAMP　*/
		(SELECT a.* FROM mst_thanks_stamp a,mst_thanks_stamp_relation b WHERE a.stamp_id = b.stamp_id AND  a.status = 0 AND a.category_id = $category_id AND b.all_flg=1) UNION
		/*　代理店共通STAMP　*/
		(SELECT a.* FROM mst_thanks_stamp a,mst_thanks_stamp_relation b WHERE a.stamp_id = b.stamp_id AND  a.status = 0 AND a.category_id = $category_id AND b.agency_id=$agency_id AND b.all_flg=0) UNION
		/*　加盟店共通STAMP　*/
		(SELECT a.* FROM mst_thanks_stamp a,mst_thanks_stamp_relation b WHERE a.stamp_id = b.stamp_id AND  a.status = 0 AND a.category_id = $category_id AND b.chain_id=$chain_id AND b.all_flg=0) UNION
		/*　店舗共通STAMP　*/
		(SELECT a.* FROM mst_thanks_stamp a,mst_thanks_stamp_relation b WHERE a.stamp_id = b.stamp_id AND  a.status = 0 AND a.category_id = $category_id AND b.shop_id=$shop_id AND b.all_flg=0) UNION
		/*　個人STAMP　*/
		(SELECT a.* FROM mst_thanks_stamp a,mst_thanks_stamp_relation b WHERE a.stamp_id = b.stamp_id AND  a.status = 0 AND a.category_id = $category_id AND b.member_id=$member_id AND b.all_flg=0)
		) c ORDER BY c.sort,c.stamp_id ASC
	");

}
function get_thanks_stamp_category_list()
{
	global $db;

	return $db->getAll("select * from mst_thanks_stamp_category where status=0 ORDER BY sort ASC");
}
function get_ranking_list($year,$month,$shop_id)
{
	global $db;

	return $db->getAll("select rank.*,IF(tm.status=2 AND tt.del_flg=0,tm.member_name,'退会済み') as member_name,IF(tm.status=2 AND tt.del_flg=0,tm.profile_img_url,'') as profile_img_url from ( SELECT a.menber_id as member_id,sum(a.thanks_receives) as thanks_receives FROM trn_thanks_cnt a where 1 and a.months=$month and a.years=$year and a.shop_id=$shop_id group by a.menber_id ,a.years,a.months) as rank left join trn_members tm on rank.member_id=tm.member_id and tm.bot_flg<>'1' LEFT JOIN trn_team tt ON rank.member_id=tt.member_id and tt.shop_id='$shop_id' order by thanks_receives desc LIMIT 0 , 10");
}
function get_thanks_cnt($member_id,$shop_id)
{
	global $db;
	return $db->getOne("select count(*) from trn_thanks where target_id =$member_id and shop_id=$shop_id ");

}
#廃止
#function get_member_array($shop_id,$from_member_id)
#{
#	global $db;
#
#	return $db->getAll("select * from trn_members where status=2 and shop_id = $shop_id and member_id <> $from_member_id");
#}
function get_thanks_info($thanks_id)
{
	global $db;

	return $db->getRow("select * from trn_thanks where thanks_id='$thanks_id'");
}
function set_reply_flg($thanks_id,$flg)
{
	global $db;
	$arr = array('reply_flg'=>$flg);
	return $db->autoExecute('trn_thanks',$arr,'UPDATE'," thanks_id= '$thanks_id'");
}
/***************************Thanks用 End****************************/
/***************************Diary用 Start****************************/
function create_diary($member_id,$shop_id,$stamp_id,$diary_contents,$img_url)
{
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$time = date('Y-m-d H:i:s',time());
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$mysqli->query("SET character_set_results = 'utf8mb4', character_set_client = 'utf8mb4', character_set_connection = 'utf8mb4', character_set_database = 'utf8mb4', character_set_server = 'utf8mb4'");

	if (!($stmt = $mysqli->prepare("INSERT INTO trn_diary (member_id,shop_id,stamp_id,diary_contents,img_url,insert_time,update_time) VALUES (?,?,?,?,?,?,?)"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}

	if (!$stmt->bind_param("sssssss", $member_id,$shop_id,$stamp_id,$diary_contents,$img_url,$time,$time)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}

	if (!$stmt->execute()) {
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$mysqli_return=mysqli_insert_id($mysqli);
	$mysqli->close();
	return $mysqli_return;
}
function update_diary_info($diary_id,$stamp_id,$diary_contents,$img_url)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('stamp_id'=>$stamp_id,'diary_contents'=>$diary_contents,'img_url'=>$img_url,'update_time'=>$time);

	return $db->autoExecute('trn_diary',$arr,'UPDATE'," diary_id= $diary_id ");
}
function delete_diary_info($diary_id)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('del_flg'=>1,'update_time'=>$time);

	return $db->autoExecute('trn_diary',$arr,'UPDATE'," diary_id= $diary_id ");
}
function get_diary_list_by_id($diary_id){
	global $db;
	return $db->getAll("select * from trn_diary where diary_id = $diary_id and del_flg=0 order by diary_id desc ");
}
function get_diary_list_by_member_limited($shop_id,$member_id){
	global $db;
	return $db->getAll("select * from trn_diary where shop_id = $shop_id and member_id=$member_id  and del_flg=0 order by diary_id desc limit 20");
}
function get_diary_list_by_member_more($shop_id,$member_id,$page,$page_number){
	global $db;
	$limit = get_limit_str($page,$page_number);
	return $db->getAll("select * from trn_diary where shop_id = $shop_id and member_id=$member_id and del_flg=0 order by diary_id desc $limit ");
}
function get_diary_list_by_shop($shop_id,$page,$page_number){
	global $db;
	$limit = get_limit_str($page,$page_number);
	return $db->getAll("select * from trn_diary where shop_id = $shop_id  and del_flg=0 order by diary_id desc $limit ");
}
function get_menber_diary_total_count($shop_id,$member_id)
{
	global $db;
	return $db->getOne("select count(*) from trn_diary where shop_id=$shop_id and member_id=$member_id  and del_flg=0 ");
}
function get_shop_diary_total_count($shop_id)
{
	global $db;
	return $db->getOne("select count(*) from trn_diary where shop_id=$shop_id  and del_flg=0 ");
}
function get_my_thanks_cnt($diary_id,$login_member_id){
	global $db;
	return $db->getOne("select count(*) from trn_diary_thanks where diary_id = $diary_id and  from_id = $login_member_id");
}
function get_same_diary_cnt($member_id,$shop_id,$stamp_id,$diary_contents,$img_url){
	global $db;
	return $db->getOne("select count(*) from trn_diary where member_id = $member_id and  shop_id = $shop_id and stamp_id = $stamp_id and diary_contents = '".$diary_contents ."' and img_url = '".$img_url."'");
}
function get_stamp_info($stamp_id)
{
	global $db;
	return $db->getRow("select * from mst_thanks_stamp where stamp_id=$stamp_id");
}
function get_diary_thanks_list($diary_id)
{
	global $db;
	return $db->getAll("select * from trn_diary_thanks where diary_id = $diary_id order by send_time desc ");
}
function send_diary_thanks($diary_id,$target_id,$stamp_id,$thanks_msg,$member_id,$shop_id,$point_status,$point_charge_status,$point_thanks_send,$point_thanks_receive)
{
	global $db2;
	$time = date('Y-m-d H:i:s',time());
	$db2->startTransaction();
	//set thanks
	$data = Array ("diary_id" => $diary_id,
			"target_id" => $target_id,
			"stamp_id" => $stamp_id,
			"thanks_msg" => $thanks_msg,
			"from_id" => $member_id,
			"send_time" => $time,
			"shop_id" => $shop_id
	);
	$id = $db2->insert ('trn_diary_thanks', $data);
	if($id){
		$check_cnt=get_point_trade_before_count_diary($member_id,$target_id,$shop_id,"3",$diary_id);
//		$check_cnt2=get_point_trade_before_count_diary($member_id,$target_id,$shop_id,"4",$diary_id);
		if($point_status=="0"&&$point_charge_status=="0"&&$target_id!=$member_id&&$check_cnt=="0"){
			if($point_thanks_send!="0"){
				//ポイント獲得(送信者にポイント付与)
				$var=add_point($db2,$member_id,$shop_id,'3',$diary_id,$point_thanks_send);
			}else{
				$var=$diary_id;
			}
			if($var){
				if($point_thanks_receive!="0"){
					//ポイント獲得(受信者にポイント付与)
					$var=add_point($db2,$target_id,$shop_id,'4',$diary_id,$point_thanks_receive);
				}

				if($var){
					$db2->commit();
				}else{
					$db2->rollback();
					$id=null;
				}
			}else{
				$db2->rollback();
				$id=null;
			}
		}else{
			$db2->commit();
		}
	}else{
		$db2->rollback();
		$id=null;
	}
	return $id;
}
function save_diary_notice_info($diary_id,$target_id,$notice_msg,$member_id,$shop_id)
{

	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$time = date('Y-m-d H:i:s',time());
	$notice_type=1;
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$mysqli->query("SET character_set_results = 'utf8mb4', character_set_client = 'utf8mb4', character_set_connection = 'utf8mb4', character_set_database = 'utf8mb4', character_set_server = 'utf8mb4'");

	if (!($stmt = $mysqli->prepare("INSERT INTO trn_notice (diary_id,target_id,notice_msg,from_id,send_time,shop_id,notice_type) VALUES (?,?,?,?,?,?,?)"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$stmt->bind_param("sssssss",$diary_id,$target_id,$notice_msg,$member_id,$time,$shop_id,$notice_type)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	if (!$stmt->execute()) {
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$mysqli_return=mysqli_insert_id($mysqli);
	$mysqli->close();
	return $mysqli_return;
}

function get_diary_info($diary_id)
{
	global $db;
	return $db->getRow(" SELECT * FROM trn_diary WHERE diary_id=$diary_id ");
}
/***************************Diary用 End****************************/

/***************************お知らせ用 Start****************************/
function creat_notice($target_id,$notice_msg,$member_id,$shop_id)
{
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$time = date('Y-m-d H:i:s',time());
	$notice_type=2;
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$mysqli->query("SET character_set_results = 'utf8mb4', character_set_client = 'utf8mb4', character_set_connection = 'utf8mb4', character_set_database = 'utf8mb4', character_set_server = 'utf8mb4'");

	if (!($stmt = $mysqli->prepare("INSERT INTO trn_notice (target_id,notice_msg,from_id,send_time,shop_id,notice_type) VALUES (?,?,?,?,?,?)"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}

	if (!$stmt->bind_param("ssssss", $target_id,$notice_msg,$member_id,$time,$shop_id,$notice_type)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}

	if (!$stmt->execute()) {
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$mysqli_return=mysqli_insert_id($mysqli);
	$mysqli->close();
	return $mysqli_return;
}
function send_push_notice($target_member_info,$msg,$type,$notice_flg){
	$var="";
	if ($target_member_info) {
		if ($target_member_info['device_type'] == "1") {
			if ($notice_flg == "0" && $target_member_info['device_id'] != "") {
				$badge = get_unread_count($target_member_info['member_id'],$target_member_info['shop_id']);
//				$var = send_push_ios($msg, $target_member_info['device_id'], $badge);
				$var = send_push_ios_with_type($msg, $target_member_info['device_id'], $badge,$type);
			}
		} else {
			if ($notice_flg == "0" && $target_member_info['device_id'] != "") {
				$registatoin_ids = array($target_member_info['device_id']);
//				$msg = array("msg" => $msg);
//				$var = send_push_android($msg, $registatoin_ids);
				$var = send_push_android_with_type($msg, $registatoin_ids,$type);
			}
		}
	}
	return $var;
}
/***************************お知らせ用 Start****************************/
function get_receive_notice_list($member_id,$shop_id,$page,$page_number)
{
	global $db;

	$limit = get_limit_str($page,$page_number);

	return $db->getAll("select a.*,IF((tm.status=2 or tm.bot_flg='1') AND tt.del_flg=0,tm.member_name,'退会済み') as member_name,IF((tm.status=2 or tm.bot_flg='1') AND tt.del_flg=0,tm.profile_img_url,'') as profile_img_url,tm.status as from_member_status  from trn_notice as a LEFT JOIN trn_members as tm ON a.target_id=tm.member_id LEFT JOIN trn_team tt ON a.target_id=tt.member_id AND a.shop_id=tt.shop_id where 1 and a.target_id = $member_id and a.shop_id= $shop_id and a.del_flg=0 order by notice_id desc $limit");
}
function get_receive_unread_notice_list($member_id,$shop_id)
{
	global $db;

	$limit = " LIMIT 0,".PAGE_MAX;

	return $db->getAll("select a.*,IF((tm.status=2 or tm.bot_flg='1') AND tt.del_flg=0,tm.member_name,'退会済み') as member_name,IF((tm.status=2 or tm.bot_flg='1') AND tt.del_flg=0,tm.profile_img_url,'') as profile_img_url,tm.status as from_member_status  from trn_notice as a LEFT JOIN trn_members as tm ON a.target_id=tm.member_id LEFT JOIN trn_team tt ON a.target_id=tt.member_id AND a.shop_id=tt.shop_id where 1 and a.from_id=tm.member_id and a.target_id = $member_id and a.shop_id= $shop_id and a.status=0 and a.del_flg=0 order by notice_id desc $limit");
}
function update_notice_read_flg($notice_id)
{
	global $db;

	$time = date('Y-m-d H:i:s',time());

	$arr = array('status'=>'1','read_time'=>$time);

	return $db->autoExecute('trn_notice',$arr,'UPDATE'," notice_id= $notice_id and status=0");
}
function update_all_notice_read_flg($member_id,$shop_id)
{
	global $db;

	$time = date('Y-m-d H:i:s',time());

	$arr = array('status'=>'1','read_time'=>$time);

	return $db->autoExecute('trn_notice',$arr,'UPDATE'," target_id= $member_id and shop_id= $shop_id and status=0");
}
function get_receive_notice_total_count($member_id,$shop_id)
{
	global $db;

//	return $db->getOne("select count(*) from trn_notice where target_id = $member_id and shop_id=$shop_id and del_flg=0");
	return $db->getOne("SELECT count(*) FROM trn_notice a where NOT EXISTS (SELECT b.member_id from trn_members b inner join trn_team c on b.member_id=c.member_id WHERE a.from_id=b.member_id and (b.status<>2 or c.del_flg=1) ) and a.target_id=$member_id and a.shop_id=$shop_id and a.del_flg=0 ");
}
function get_unread_notice_count($member_id,$shop_id)
{
	global $db;

//	return $db->getOne("select count(*) from trn_notice where target_id =$member_id and shop_id=$shop_id and status=0 and del_flg=0");
	return $db->getOne("SELECT count(*) FROM trn_notice a where NOT EXISTS (SELECT b.member_id from trn_members b inner join trn_team c on b.member_id=c.member_id WHERE a.from_id=b.member_id and (b.status<>2 or c.del_flg=1) ) and a.target_id=$member_id and a.shop_id=$shop_id and a.status=0 and a.del_flg=0 ");

}
/***************************お知らせ用 End****************************/
/***************************チーム切替用 Start****************************/
function add_team_info($member_id,$shop_id)
{

	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$time = date('Y-m-d H:i:s',time());
	$active_flg = 0;
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$mysqli->query("SET character_set_results = 'utf8mb4', character_set_client = 'utf8mb4', character_set_connection = 'utf8mb4', character_set_database = 'utf8mb4', character_set_server = 'utf8mb4'");

	if (!($stmt = $mysqli->prepare("INSERT INTO trn_team (shop_id,member_id,active_flg) VALUES (?,?,?) ON DUPLICATE KEY UPDATE del_flg=0"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$stmt->bind_param("ssd", $shop_id,$member_id,$active_flg)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	if (!$stmt->execute()) {
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$mysqli_return=mysqli_insert_id($mysqli);
	$mysqli->close();
	return $mysqli_return;
}
function active_shop($member_id,$shop_id){
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('shop_id'=>$shop_id,'final_login_time'=>$time,'update_time'=>$time);

	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= $member_id");
}
//function update_member_shop_info($member_id,$shop_id){
//	global $db;
//	$time = date('Y-m-d H:i:s',time());
//
//	$arr = array('shop_id'=>$shop_id,'final_login_time'=>$time,'update_time'=>$time);
//
//	return $db->autoExecute('trn_members',$arr,'UPDATE'," member_id= $member_id");
//}
function delete_team_info($member_id,$shop_id)
{
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('del_flg'=>1,'update_time'=>$time);

	return $db->autoExecute('trn_team',$arr,'UPDATE'," member_id= $member_id and shop_id=$shop_id and active_flg=0");
}
function get_team_list($member_id)
{
	global $db;
	return $db->getAll("select * from trn_team where member_id = $member_id and del_flg=0");
}
function get_duplicate_shop_id_count($shop_id,$member_id){
	global $db;
	return $db->getOne("select count(*) from trn_team where shop_id = $shop_id and member_id=$member_id and del_flg=0");
}
/***************************チーム切替用 End****************************/
/***************************共通処理用 Start****************************/
function get_online_version($os){
	global $db;

	return $db->getOne("select online_version from mst_online_version where os_flg=$os ");
}
function get_mvp_member_info($shop_id,$month,$year)
{
	global $db;

	return $db->getRow("SELECT max(sp.thanks_receives),sp.* from (select rank.*,IF(tm.status=2 AND tt.del_flg=0,tm.member_name,'退会済み') as member_name,IF(tm.status=2 AND tt.del_flg=0,tm.profile_img_url,'') as profile_img_url from ( SELECT a.menber_id as member_id,sum(a.thanks_receives) as thanks_receives FROM trn_thanks_cnt a where 1 and a.months='".$month."' and a.years='".$year."' and a.shop_id=$shop_id group by a.menber_id ,a.years,a.months ORDER by thanks_receives desc) as rank left join trn_members tm on rank.member_id=tm.member_id LEFT JOIN trn_team tt ON rank.member_id=tt.member_id and tt.shop_id=$shop_id) sp");
}
function get_setting()
{
	global $db;
//	return $db->getRow("select * from mst_setting");
//	return $db->getAll("select * from mst_setting where setting_name like '%thanks_lever_up_%' order by setting_value");
//	return $db->getRow("select * from mst_setting where setting_value > $thanks_cnt order by setting_value limit 1");
	return $db->getAll(" SELECT setting_value FROM mst_setting WHERE setting_name LIKE 'thanks_lever_up_%'ORDER BY (setting_value+0) ASC ");
}
function get_theme_info($shop_id,$agency_id,$chain_id)
{
	global $db;
	return $db->getRow("select mt.* from mst_theme mt,mst_theme_relation mtr where mt.theme_id=mtr.theme_id and mtr.shop_id=$shop_id and mtr.agency_id=$agency_id and mtr.chain_id=$chain_id");

}
/***************************共通処理用 End****************************/
/***************************bot用 Start****************************/
function get_bot_list(){
	global $db;
	return $db->getAll("select * from mst_diary_bot where status=0");
}
function get_bot_relation_list($now_hour){
	global $db;
	return $db->getAll("SELECT DISTINCT dbr.shop_id,dbr.bot_id,dbr.send_cycle_month_date,dbr.send_cycle_week_day,dbr.send_cycle_day_once,dbr.send_method,dbr.send_loop_flg,dbr.execute_timeh,dbr.bot_member_id FROM mst_diary_bot_relation as dbr INNER JOIN mst_diary_bot as mdb ON dbr.bot_id = mdb.bot_id AND mdb.status=0 WHERE dbr.status=0 AND dbr.execute_timeh='$now_hour'");
}
function get_bot_last_content_info($shop_id,$bot_id){
	global $db;
	return $db->getRow("SELECT  dbr.last_send_contents_id,last_send_time FROM mst_diary_bot_relation as dbr WHERE dbr.shop_id='$shop_id' AND dbr.bot_id='$bot_id'  LIMIT 1");
}
function get_bot_max_contents_id($shop_id,$bot_id){
	global $db;
	return $db->getOne("SELECT  MAX(dbr.contents_id) FROM mst_diary_bot_relation as dbr INNER JOIN mst_diary_bot_contents as mdbc ON dbr.contents_id = mdbc.contents_id AND mdbc.status=0 WHERE dbr.shop_id='$shop_id' AND dbr.bot_id='$bot_id'");
}

function get_bot_contents_id($shop_id,$bot_id,$last_send_contents_id){
	global $db;
	return $db->getOne("SELECT  dbr.contents_id FROM mst_diary_bot_relation as dbr WHERE dbr.shop_id='$shop_id' AND dbr.bot_id='$bot_id' AND dbr.contents_id>'$last_send_contents_id' ORDER BY dbr.contents_id ASC LIMIT 1");
}
function get_bot_content_info($bot_contents_id){
	global $db;
	return $db->getRow("SELECT dbc.contents_id,dbc.stamp_id,dbc.contents,dbc.img_url FROM mst_diary_bot_contents dbc WHERE dbc.contents_id='$bot_contents_id' AND dbc.status=0 ");
}
function get_bot_content_random($last_send_contents_id){
	global $db;
	return $db->getRow("select * from mst_diary_bot_contents where status=0 and contents_id<>$last_send_contents_id ORDER BY RAND() LIMIT 1");
}
function init_bot_last_send_id($shop_id, $bot_id){
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('last_send_contents_id'=>0,'last_send_time'=>$time);
	return $db->autoExecute('mst_diary_bot_relation',$arr,'UPDATE'," shop_id= $shop_id and bot_id= $bot_id");
}
function update_bot_send_info($shop_id, $bot_id,$contents_id){
	global $db;
	$time = date('Y-m-d H:i:s',time());

	$arr = array('last_send_contents_id'=>$contents_id,'last_send_time'=>$time);
	return $db->autoExecute('mst_diary_bot_relation',$arr,'UPDATE'," shop_id= $shop_id and bot_id= $bot_id and status=0" );
}

/***************************bot用 End****************************/
/***************************ポイント用 Start****************************/
function get_point_total($member_id,$shop_id)
{
	global $db;

	return $db->getRow("SELECT SUM(IF(trade_kind<50,trade_point,0)) AS total_income_points,SUM(IF(trade_kind>=50,-trade_point,0)) AS total_cost_points FROM trn_point_trade WHERE member_id=$member_id AND shop_id=$shop_id AND status=0 ");
}
function get_point_holding($member_id,$shop_id)
{
	global $db;

	return $db->getRow("SELECT SUM(trade_point) AS holding_points FROM trn_point_trade WHERE member_id=$member_id AND shop_id=$shop_id AND status=0 ");
}
function get_monthly_exchange_info($member_id,$shop_id)
{
	global $db;
	$begin_date_time= date('Y-m-01 00:00:00', strtotime(date("Y-m-d")));
	$last_date_time= date('Y-m-d 23:59:59', strtotime("$begin_date_time +1 month -1 day"));
	return $db->getRow("SELECT SUM(exchange_point) AS monthly_exchange_point FROM trn_point_exchange WHERE member_id=$member_id AND shop_id=$shop_id AND request_datetime BETWEEN '$begin_date_time' AND '$last_date_time' AND COALESCE(exchange_result,0)<>1 ");
}
function get_point_trade_list($member_id,$shop_id,$page,$page_number)
{
	global $db;

	$limit = get_limit_str($page,$page_number);

	return $db->getAll("SELECT concat(trade_year,'/',trade_month) AS summary_date, SUM(IF(trade_kind=1,trade_point,0)+IF(trade_kind=3,trade_point,0))+SUM(IF(trade_kind=2,trade_point,0)+IF(trade_kind=4,trade_point,0)) AS total_receive_points,SUM(IF(trade_kind=1,trade_point,0)+IF(trade_kind=3,trade_point,0)) AS total_points_by_send,SUM(IF(trade_kind=2,trade_point,0)+IF(trade_kind=4,trade_point,0)) AS total_points_by_receive FROM trn_point_trade WHERE member_id = $member_id AND shop_id = $shop_id AND status = 0 GROUP BY trade_year, trade_month ORDER BY trade_year, trade_month desc $limit");
}
function get_receive_point_total_count($member_id,$shop_id)
{
	global $db;

	return $db->getOne("SELECT COUNT(*) FROM trn_point_trade WHERE member_id = $member_id AND shop_id = $shop_id AND status = 0 AND trade_kind < 50 ");
}
function get_point_exchange_list($member_id,$shop_id,$page,$page_number)
{
	global $db;

	$limit = get_limit_str($page,$page_number);

	return $db->getAll("SELECT DATE_FORMAT(tpe.request_datetime,'%Y/%m/%d %T') AS exchange_time,CASE WHEN (tpe.status=0 OR tpe.status =1) THEN 1 WHEN tpe.status=2 AND COALESCE(tpe.exchange_result,9)=0 THEN 2 WHEN tpe.status=2 AND COALESCE(tpe.exchange_result,9)=1  THEN 3 END as exchange_status,tpe.exchange_point AS exchange_point,mpi.item_name AS exchange_item_name,tpe.exchange_resultdetail AS exchange_result_detail FROM trn_point_exchange tpe INNER JOIN mst_point_item mpi ON tpe.item_id=mpi.item_id WHERE tpe.member_id = $member_id AND tpe.shop_id= $shop_id ORDER BY exchange_id desc $limit");
}
function get_point_exchange_total_count($member_id,$shop_id)
{
	global $db;

	return $db->getOne("select count(*) from trn_point_exchange where member_id = $member_id and shop_id=$shop_id");
}
function get_point_item_list($shop_id,$page,$page_number)
{
	global $db;

	$limit = get_limit_str($page,$page_number);

	return $db->getAll("SELECT mpi.* FROM mst_point_item mpi INNER JOIN mst_point_item_relation mpir ON mpi.item_id=mpir.item_id WHERE mpi.status=0 AND mpir.shop_id=$shop_id ORDER BY mpi.sort asc $limit");
}
function get_point_item_total_count()
{
	global $db;

	return $db->getOne("select count(*) from mst_point_item where status=0");
}
function get_point_item_info($item_id)
{
	global $db;

	return $db->getRow("SELECT item_id, item_name, image_url, item_point,COALESCE(item_text,'') AS item_text,COALESCE(target_name,'') AS target_name,COALESCE(target_text,'') AS target_text, sort, status, insert_time, update_time FROM mst_point_item WHERE item_id=$item_id AND status=0");
}
function get_point_item_target_value($item_id,$member_id,$shop_id)
{
	global $db;

	return $db->getRow("SELECT target_value FROM trn_point_exchange WHERE item_id = $item_id and member_id = $member_id and shop_id=$shop_id ORDER BY request_datetime DESC LIMIT 0,1 ");
}
function send_point_exchange_info($member_id,$shop_id,$item_id,$exchange_point,$target_value)
{
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$time = date('Y-m-d H:i:s',time());
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$mysqli->query("SET character_set_results = 'utf8mb4', character_set_client = 'utf8mb4', character_set_connection = 'utf8mb4', character_set_database = 'utf8mb4', character_set_server = 'utf8mb4'");

	if (!($stmt = $mysqli->prepare("INSERT INTO trn_point_exchange (member_id,shop_id,item_id,exchange_point,request_datetime,target_value) VALUES (?,?,?,?,?,?)"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}

	if (!$stmt->bind_param("ssssss", $member_id,$shop_id,$item_id,$exchange_point,$time,$target_value)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}

	if (!$stmt->execute()) {
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$mysqli_return=mysqli_insert_id($mysqli);
	$mysqli->close();
	return $mysqli_return;
}
function send_point_trade_info($member_id,$shop_id,$trade_target,$trade_point)
{
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$time = date('Y-m-d H:i:s',time());
	$trade_year=date("Y");
	$trade_month=date("m");
	$trade_day=date("d");
	$trade_kind='50';
	$trade_point=-$trade_point;
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$mysqli->query("SET character_set_results = 'utf8mb4', character_set_client = 'utf8mb4', character_set_connection = 'utf8mb4', character_set_database = 'utf8mb4', character_set_server = 'utf8mb4'");

	if (!($stmt = $mysqli->prepare("INSERT INTO trn_point_trade (member_id,shop_id,trade_kind,trade_target,trade_year,trade_month,trade_day,trade_point) VALUES (?,?,?,?,?,?,?,?)"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}

	if (!$stmt->bind_param("ssssssss", $member_id,$shop_id,$trade_kind,$trade_target,$trade_year,$trade_month,$trade_day,$trade_point)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}

	if (!$stmt->execute()) {
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$mysqli_return=mysqli_insert_id($mysqli);
	$mysqli->close();
	return $mysqli_return;
}
function get_point_exchange_result($member_id,$shop_id)
{
	global $db;

	return $db->getRow("SELECT request_datetime,exchange_id,exchange_result,COALESCE(exchange_resultdetail,'') AS exchange_result_detail FROM trn_point_exchange WHERE member_id=$member_id AND status=2 AND read_status=0 LIMIT 1");
}
function set_read_status($exchange_id)
{
	global $db;
	$arr = array('read_status'=>1);
	return $db->autoExecute('trn_point_exchange',$arr,'UPDATE'," exchange_id= '$exchange_id'");
}
function set_trade_read_status($member_id,$shop_id,$target,$trade_kind)
{
	global $db;
	if($target!=""){$target=" AND trade_target = $target ";}else{$target="";}
	if($trade_kind!=""){$trade_kind=" AND trade_kind = $trade_kind ";}else{$trade_kind="";}
	$arr = array('read_status'=>1);
	return $db->autoExecute('trn_point_trade',$arr,'UPDATE'," member_id = $member_id AND shop_id = $shop_id $target $trade_kind AND status = 0 AND read_status=0 AND trade_kind < 50 ");
}
function get_unread_trade_point_cnt($member_id,$shop_id,$target,$trade_kind)
{
	global $db;
	//Targetが指定された場合は、Target対象のみ　//取引ターゲット。Thanks!送受信の場合はThanks!ID、日記Thanks!、日記投稿の場合は日記ID,MVPの場合はMVP日付yyyymmdd、ポイント交換の場合は交換ID
//	$target=($target!="")?" AND trade_target = $target ":"";
//	$trade_kind=($trade_kind!="")?" AND trade_kind = $trade_kind ":"";
	if($target!=""){$target=" AND trade_target = $target ";}else{$target="";}
	if($trade_kind!=""){$trade_kind=" AND trade_kind = $trade_kind ";}else{$trade_kind="";}

	return $db->getOne("SELECT COALESCE(SUM(trade_point),0) AS trade_point FROM trn_point_trade WHERE member_id = $member_id AND shop_id = $shop_id $target $trade_kind AND status = 0 AND read_status=0 AND trade_kind < 50 ");
}
function add_point($db2,$member_id,$shop_id,$trade_kind,$trade_target,$trade_point)
{
//    global $db2;
	$trade_year=date("Y");
	$trade_month=date("m");
	$trade_day=date("d");
    //set point sub function
    $data = Array ("member_id" => $member_id,
               "shop_id" => $shop_id,
               "trade_kind" => $trade_kind,
               "trade_target" => $trade_target,
               "trade_year" => $trade_year,
               "trade_month" => $trade_month,
               "trade_day" => $trade_day,
               "trade_point" =>$trade_point
    );
    return $db2->insert ('trn_point_trade', $data);
        
}
function get_point_trade_before_count($member_id,$target_id,$shop_id,$trade_kind)
{
	global $db;
	$trade_year=date("Y");
	$trade_month=date("m");
	$trade_day=date("d");

	if($trade_kind=="1"){
		return $db->getOne("SELECT count(1) FROM trn_point_trade trd INNER JOIN trn_thanks tks ON trd.trade_target=tks.thanks_id AND tks.target_id=$target_id WHERE trd.member_id=$member_id AND trd.shop_id=$shop_id AND trd.trade_kind=$trade_kind AND trd.trade_year=$trade_year AND trd.trade_month=$trade_month AND trd.trade_day=$trade_day AND trd.status=0");
	}
	if($trade_kind=="2"){
		return $db->getOne("SELECT count(1) FROM trn_point_trade trd INNER JOIN trn_thanks tks ON trd.trade_target=tks.thanks_id AND tks.from_id=$member_id WHERE trd.member_id=$member_id AND trd.shop_id=$shop_id AND trd.trade_kind=$trade_kind AND trd.trade_year=$trade_year AND trd.trade_month=$trade_month AND trd.trade_day=$trade_day AND trd.status=0");
	}
}
function get_point_trade_before_count_diary($member_id,$target_id,$shop_id,$trade_kind,$diary_id)
{
	global $db;
	$trade_year=date("Y");
	$trade_month=date("m");
	$trade_day=date("d");

	if($trade_kind=="3"){
//		return $db->getOne("SELECT count(1) FROM trn_point_trade trd INNER JOIN trn_diary_thanks tks ON trd.trade_target=tks.diary_id AND tks.target_id=$target_id WHERE trd.member_id=$member_id AND trd.shop_id=$shop_id AND trd.trade_kind=$trade_kind AND trd.trade_target=$diary_id AND trd.trade_year=$trade_year AND trd.trade_month=$trade_month AND trd.trade_day=$trade_day AND trd.status=0");
		return $db->getOne("SELECT count(1) FROM trn_point_trade trd INNER JOIN trn_diary_thanks tks ON trd.trade_target=tks.diary_id AND tks.target_id=$target_id WHERE trd.member_id=$member_id AND trd.shop_id=$shop_id AND trd.trade_kind=$trade_kind AND trd.trade_target=$diary_id AND trd.status=0");
	}
	if($trade_kind=="4"){
//		return $db->getOne("SELECT count(1) FROM trn_point_trade trd INNER JOIN trn_diary_thanks tks ON trd.trade_target=tks.diary_id AND tks.from_id=$member_id WHERE trd.member_id=$member_id AND trd.shop_id=$shop_id AND trd.trade_kind=$trade_kind AND trd.trade_target=$diary_id AND trd.trade_year=$trade_year AND trd.trade_month=$trade_month AND trd.trade_day=$trade_day AND trd.status=0");
		return $db->getOne("SELECT count(1) FROM trn_point_trade trd INNER JOIN trn_diary_thanks tks ON trd.trade_target=tks.diary_id AND tks.from_id=$member_id WHERE trd.member_id=$member_id AND trd.shop_id=$shop_id AND trd.trade_kind=$trade_kind AND trd.trade_target=$diary_id AND trd.status=0");
	}
}
function update_point_exchange_info($exchange_id,$trade_id)
{
	global $db;
	$arr = array('trade_id'=>$trade_id);
	return $db->autoExecute('trn_point_exchange',$arr,'UPDATE'," exchange_id= '$exchange_id'");
}
/***************************ポイント用 End****************************/
?>