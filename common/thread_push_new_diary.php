<?php
include('include/init.php');
//メンバーID
$member_id = $argv[1];
//チームID
$shop_id = $argv[2];
//日記ID
$diary_id = $argv[3];
//チームの皆にPush通知
//			$member_array = get_member_array($shop_id,$member_id);
$member_array = get_shop_member_list($shop_id);
$shop_info = get_shop_info($shop_id);
if ($member_array) {
	error_log("スレッドでPUSH通知処理開始！". date('Y/m/d H:i:s')."\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
	$notice_msg="日記に新しい投稿がありました。皆でThanks!を贈りましょう！";
	$notice_msg= html_tag_chg(SBC_DBC(trim(urldecode($notice_msg)),0));//Noticeメッセージ
	error_log("スレッドでPUSH通知メッセージ：". $notice_msg."\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
	foreach ($member_array as $key => $target_member_info) {
		if ($target_member_info['status'] == "2" && $shop_info['status'] == "0"&&$target_member_info['diary_notice_flg'] == "0") {
				if($target_member_info['member_id']!=$member_id){
					save_diary_notice_info($diary_id,$target_member_info['member_id'],$notice_msg,$member_id,$shop_id);
					send_push_notice($target_member_info,$notice_msg,"1",$target_member_info['diary_notice_flg']);//お知らせ一覧に遷移
					error_log("スレッドでPUSH通知処理：日記ID=".$diary_id."宛先ID=".$target_member_info['member_id'] ."チームID=".$shop_id. "\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
				}
		}
	}
	error_log("スレッドでPUSH通知処理終了！". date('Y/m/d H:i:s')."\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
}

?>