<?php
include('include/init.php');
//メンバーID
$member_id = $argv[1];
//チームID
$shop_id = $argv[2];
//日記ID
$stamp_id = $argv[3];
//サンクスメッセージ
$thanks_msg = $argv[4];
if ($thanks_msg == '') {
	$thanks_msg = "Thanks!スタンプが届きました！";
}

//全員に送信
$member_array = get_shop_member_list($shop_id);
$shop_info = get_shop_info($shop_id);
$login_member_info = get_member_info($member_id);
$login_shop_info = get_shop_info($login_member_info['shop_id']);
//		print_r($member_array);die;
if ($member_array) {
	error_log("スレッドでPUSH通知処理開始！". date('Y/m/d H:i:s')."\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
	if($login_member_info['status'] == "2" && $login_shop_info['status'] == "0") {
		error_log("スレッドでPUSH通知メッセージ：". $thanks_msg."\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
		foreach ($member_array as $key => $member_info) {
			if ($login_member_info['status'] == "2" && $login_shop_info['status'] == "0" && $member_info['status'] == "2" && $shop_info['status'] == "0" && $member_info['member_id'] != $member_id) {
				if ($member_info) {
					if ($member_info['device_type'] == "1") {
						if ($member_info['thanks_notice_flg'] == "0" && $member_info['device_id'] != "") {
							$badge = get_unread_count($member_info['member_id'], $member_info['shop_id']);
							$var = send_push_ios_with_type($thanks_msg, $member_info['device_id'], $badge,"3");//獲得Thanks!一覧に遷移
						}
					} else {
						if ($member_info['thanks_notice_flg'] == "0" && $member_info['device_id'] != "") {
							$registatoin_ids = array($member_info['device_id']);
							$var = send_push_android_with_type($thanks_msg, $registatoin_ids,"3");//獲得Thanks!一覧に遷移
						}
					}
					error_log("スレッドでPUSH通知処理：宛先ID=".$member_info['member_id'] ."送信元ID=".$member_id."チームID=".$shop_id. "\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
				}
			}
		}
	}
	else {
		if ($login_shop_info['status'] != "0" || $shop_info['status'] != "0") {
			error_log("現在このチームのThanks!は利用停止中です。\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
		}
		else {
			error_log("このユーザーは退会済みです。\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
		}
	}
	error_log("スレッドでPUSH通知処理終了！". date('Y/m/d H:i:s')."\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
}
?>