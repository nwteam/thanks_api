<?php
include('include/init.php');
	//メンバーID
	$member_id = $argv[1];
	//チームID
	$shop_id = $argv[2];
	//Push通知<ユーザー名>さんがチームに参加しました。 ウェルカムThanks!を贈ろう！
	$member_array = get_shop_member_list($shop_id);
	$reg_member_info = get_shop_member_info($member_id,$shop_id);
	if ($member_array) {
		error_log("スレッドでPUSH通知処理開始！". date('Y/m/d H:i:s')."\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
		$notice_msg=$reg_member_info['member_name']."さんがチームに参加しました。 ウェルカムThanks!を贈ろう！ ";
		$notice_msg= html_tag_chg(SBC_DBC(trim(urldecode($notice_msg)),0));//Noticeメッセージ
		error_log("スレッドでPUSH通知メッセージ：". $notice_msg."\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
		foreach ($member_array as $key => $target_member_info) {
			if ($target_member_info['status'] == "2"&&$target_member_info['birthday_notice_flg'] == "0") {
					if($target_member_info['member_id']!=$member_id){
						#通知設定有無にかかわらずお知らせ情報は作れる
						$notice_id =creat_notice($target_member_info['member_id'],$notice_msg,$member_id,$shop_id);
						send_push_notice($target_member_info,$notice_msg,"1",$target_member_info['birthday_notice_flg']);
						error_log("スレッドでPUSH通知処理：宛先ID=".$target_member_info['member_id'] ."チームID=".$shop_id. "\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
					}
			}
		}
		error_log("スレッドでPUSH通知処理終了！". date('Y/m/d H:i:s')."\r\n", 3, DOCUMENT_ROOT.'/log/thread_push.log');
	}
?>