<?php
switch($a)
{
	//登録状態を検知するAPI
	case 'status_chk':
		check_parameter(array($member_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		//check_sign($_GET,$sign);

		$info=get_member_status($member_id);
		if(!$info){
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}else{
			rencode(0,'',$info);
		}
		break;
	//メールアドレス、パスワード登録
	case 'reg_mail':
		check_parameter(array($email,$password));
		$email= html_tag_chg(SBC_DBC(trim(urldecode($email)),0));//メールアドレス
		$password = trim($password);//パスワード
		$password =md5($password);
//		check_sign($_GET,$sign);

		if(check_email($email)){
			$member_info = get_member_info_by_email($email);
			if($member_info['status']=='2'||$member_info['bot_flg']=='1'){
				rencode(1,'メールアドレスが既に存在しました。');
			}else {
				rencode(0,'続いて登録完了まで登録してください。',$member_info['member_id'],'member_id');
			}
		}else{
			$member_id = reg_mail($email,$password);
			if($member_id > 0){
				rencode(0,'登録に成功しました。',$member_id,'member_id');
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}
		break;
	//プロフィール登録API
	case 'reg_profile':
		check_parameter(array($member_id,$member_name,$service_start_day));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$member_name= html_tag_chg(SBC_DBC(trim(urldecode($member_name)),0));//名前
		$birthday=html_tag_chg(SBC_DBC(trim($birthday),0));//生年月日 1999-09-09
		$service_start_day=html_tag_chg(SBC_DBC(trim($service_start_day),0));//勤務開始日 1999-09-09
		$self_introduction= html_tag_chg(SBC_DBC(trim(urldecode($self_introduction)),0));//自己紹介
		$interest= html_tag_chg(SBC_DBC(trim(urldecode($interest)),0));//趣味・好きなもの
		$image_url= html_tag_chg(SBC_DBC(trim(urldecode($image_url)),0));//画像URL
//		check_sign($_GET,$sign);

		$var = reg_profile($member_id,$member_name,$birthday,$service_start_day,$self_introduction,$interest,$image_url);
		if($var){
			rencode(0,'プロフィールを登録しました。');
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		break;
	//新規登録機能
	case 'reg_shop':
		check_parameter(array($member_id,$shop_auth_code,$device_type,$device_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$shop_auth_code= html_tag_chg(SBC_DBC(trim(urldecode($shop_auth_code)),0));//チーム認証用コード
		$device_type = trim($device_type);//デバイスタイプ　1:iOS,2:Android
		$device_id = trim($device_id);//パスワード
//		check_sign($_GET,$sign);
		$shop_info = check_shop_auth_code($shop_auth_code);
		$shop_id=$shop_info['id'];
		if(!$shop_info)
		{
			rencode(1,'該当チームが存在しません。');
		}else {
			$member_info = get_member_info_by_email($email);
			if($member_info['status']=='2'||$member_info['bot_flg']=='1'){
				rencode(1,'既に登録済みです。');
			}
			if($shop_info['status']=="0"){
				$var = reg_shop($member_id, $shop_info['id'],$device_type,$device_id);
				$exec_file=DOCUMENT_ROOT.VERSION_PATH.'common/thread_push_reg_shop.php';
				if ($var) {
					exec("nohup php -c '' $exec_file $member_id $shop_id  > /dev/null &");
//					//Push通知<ユーザー名>さんがチームに参加しました。 ウェルカムThanks!を贈ろう！
//					$member_array = get_shop_member_list($shop_info['id']);
//					$reg_member_info = get_shop_member_info($member_id,$shop_info['id']);
//					if ($member_array) {
//						foreach ($member_array as $key => $target_member_info) {
//							if ($target_member_info['status'] == "2"&&$target_member_info['birthday_notice_flg'] == "0") {
//								$notice_msg=$reg_member_info['member_name']."さんがチームに参加しました。 ウェルカムThanks!を贈ろう！ ";
//								$notice_msg= html_tag_chg(SBC_DBC(trim(urldecode($notice_msg)),0));//Noticeメッセージ
//								if($var){
//									if($target_member_info['member_id']!=$member_id){
//										#通知設定有無にかかわらずお知らせ情報は作れる
//										$notice_id =creat_notice($target_member_info['member_id'],$notice_msg,$member_id,$shop_info['id']);
//										send_push_notice($target_member_info,$notice_msg,"1",$target_member_info['birthday_notice_flg']);
//									}
//								}
//							}
//						}
//					}
					# 2017/09/26 osonoi@ism edit_start
					$member_info = get_member_info($member_id);
					$to = $member_info['email'];
					$subject = '【thanks!】ご登録ありがとうございます';
					$body = <<<EOF
{$member_info["member_name"]}様

この度は、「thanks!」にご登録頂きまして誠にありがとうございます。
職場のチームワークの更なる向上にぜひご活用ください。
まずは、既に登録しているメンバーの皆に登録完了をお知らせするthanks!を送ってみましょう！

【アカウント】
ご登録頂きましたアカウント情報は以下になります。
Mail：{$member_info["email"]}
パスワード：セキュリティー保護のため表示されません

【コンセプトムービーのお知らせ】
実際の本屋さんを舞台にしたドラマ、『スライス　オブ　thanks! ライフ』がyoutubeにて公開中です！
何気ない一言でも、言ってもらうことで少し前向きになれることってありませんか？
そんなthanks!のコンセプトを皆さんにも知っていただきたく製作しました。
ぜひ、御覧ください！

URL：https://www.youtube.com/watch?v=NYw5XUN0m3o

【お問い合わせ】
ご不明な点がございましたら、お手数ですが下記までご連絡ください。
thanks!サポート事務局：39s@aruto.me

※当メールに心当たりの無い方は、誠に恐れ入りますが破棄していただけますようお願い致します。

EOF;
					$header = 'From:'.mb_encode_mimeheader('thanks! 運営チーム').'<support@aruto.me>'."\n";
					mb_language('uni');
					mb_internal_encoding('UTF-8');
					mb_send_mail($to, $subject, $body, $header);
					# 2017/09/26 osonoi@ism edit_end
					rencode(0,'登録に成功しました。',$shop_info['id'],'shop_id');
				} else {
					rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
				}
			}else{
				rencode(1,'該当チームは利用停止中です。');
			}
		}
		break;
	case 'reg_device':
		check_parameter(array($member_id,$device_type,$device_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$device_type = trim($device_type);//デバイスタイプ　1:iOS,2:Android
		$device_id = trim($device_id);//パスワード
//		check_sign($_GET,$sign);
		$var = reg_device($member_id,$device_type,$device_id);
		if ($var) {
			rencode(0,'');
		} else {
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		break;
	//ログイン機能
	case 'login':
		check_parameter(array($email,$password,$device_type,$device_id));
		$email= html_tag_chg(SBC_DBC(trim(urldecode($email)),0));//メールアドレス
		$password = trim($password);//パスワード
		$device_type = trim($device_type);//デバイスタイプ　1:iOS,2:Android
		$device_id = trim($device_id);//パスワード
//		check_sign($_GET,$sign);

		$login_info = get_login_info($email);
//		$shop_info = get_shop_info($login_info['shop_id']);
		if(!$login_info){
			rencode(1,'メールアドレスまたはパスワードに誤りがあります。');
		}else{
			if(md5($password) != $login_info['password']){
				rencode(1,'メールアドレスまたはパスワードに誤りがあります。');
			}else{
				if($login_info['status']=="2"){
					$var = update_device_info($login_info['member_id'],$device_type,$device_id);
					$arr['member_id']=$login_info['member_id'];
					$arr['shop_id']=$login_info['shop_id'];
					$arr['status']=$login_info['status'];
					$arr['thanks_notice_flg']=$login_info['thanks_notice_flg'];
					$arr['birthday_notice_flg']=$login_info['birthday_notice_flg'];
					rencode(0,'ログインしました。',$arr,'member_info');
				}else{
//					if($shop_info['status']!="0"){
//						rencode(1,'現在このチームのサンクスは利用停止中です。');
//					}else{
						rencode(1,'このユーザーは退会済みです。');
//					}
				}
			}
		}
		break;
	//パスワードリマインド
	case 'pw_remind':
		break;
	//プロフィール編集処理
	case 'edit_profile':
		check_parameter(array($member_id,$member_name,$service_start_day));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$member_name= html_tag_chg(SBC_DBC(trim(urldecode($member_name)),0));//名前
		$birthday=html_tag_chg(SBC_DBC(trim($birthday),0));//生年月日 1999-09-09
		$service_start_day=html_tag_chg(SBC_DBC(trim($service_start_day),0));//勤務開始日 1999-09-09
		$self_introduction= html_tag_chg(SBC_DBC(trim(urldecode($self_introduction)),0));//自己紹介
		$interest= html_tag_chg(SBC_DBC(trim(urldecode($interest)),0));//趣味・好きなもの
		$image_url= html_tag_chg(SBC_DBC(trim(urldecode($image_url)),0));//画像URL
//		check_sign($_GET,$sign);

		$var = edit_profile($member_id,$member_name,$birthday,$service_start_day,$self_introduction,$interest,$image_url);
		if($var){
			rencode(0,'更新に成功しました。');
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		break;
	//プロフィール情報取得
	case 'profile':
		check_parameter(array($shop_id,$member_id));
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
//		check_sign($_GET,$sign);

		$member_info = get_shop_member_info($member_id,$shop_id);
		$shop_info = get_shop_info($shop_id);
		if(!$member_info){
			rencode(1,'該当ユーザーが存在しません。');
		}else{
			if($member_info['status']!='2'&&($member_info['status']=='9'&&$member_info['bot_flg']!='1')){
				$startdate=strtotime($member_info['service_start_day']);
				$enddate=strtotime(date('Y-m-d',time()));
				$days=round(($enddate-$startdate)/3600/24) ;

				$insert_time=strtotime($member_info['insert_time']);
				$today=strtotime(date('Y-m-d 23:59:59',time()));
				$sign_days=round(($today-$insert_time)/3600/24) ;

				if($sign_days>=0&&$sign_days<=7){
					$member_info ['icon_flg'] = "1";//新規ユーザー(登録後7日間)
				}elseif(($days%100>=0&&$days%100<=7&&$days!=0&&$days>=100)){
					$member_info ['icon_flg'] = "2";//勤続開始100日のメンバー 
				}elseif(substr($member_info['birthday'],5,2)==date("m",time())){
					$member_info ['icon_flg'] = "3";//今月誕生日のメンバー 
				}else{
					$member_info ['icon_flg'] = "4";//最終ログイン時間の順で取得したメンバー 
				}
				$member_info['service_days']="0";
				$member_info['birthday']="";
				$member_info['member_name']="退会済みユーザー";
				$member_info['interest']="";
				$member_info['self_introduction']="";
				$member_info['receive_thanks_cnt']="0";
				$member_info['send_thanks_cnt']="0";
				$member_info['final_login_time']="";
				$member_info['service_start_day']="";
				$member_info['profile_img_url']="";
				rencode(0,'',$member_info);
			}elseif($member_info['bot_flg']=='1'){
				$startdate=strtotime($member_info['service_start_day']);
				$enddate=strtotime(date('Y-m-d',time()));
				$days=round(($enddate-$startdate)/3600/24) ;

				$insert_time=strtotime($member_info['insert_time']);
				$today=strtotime(date('Y-m-d 23:59:59',time()));
				$sign_days=round(($today-$insert_time)/3600/24) ;

				if($sign_days>=0&&$sign_days<=7){
					$member_info ['icon_flg'] = "1";//新規ユーザー(登録後7日間)
				}elseif(($days%100>=0&&$days%100<=7&&$days!=0&&$days>=100)){
					$member_info ['icon_flg'] = "2";//勤続開始100日のメンバー
				}elseif(substr($member_info['birthday'],5,2)==date("m",time())){
					$member_info ['icon_flg'] = "3";//今月誕生日のメンバー
				}else{
					$member_info ['icon_flg'] = "4";//最終ログイン時間の順で取得したメンバー
				}
				$member_info['service_days']="0";
				$member_info['birthday']="";
//				$member_info['member_name']="退会済みユーザー";
				$member_info['interest']="";
//				$member_info['self_introduction']="";
				$member_info['receive_thanks_cnt']="0";
				$member_info['send_thanks_cnt']="0";
				$member_info['final_login_time']="";
				$member_info['service_start_day']="";
//				$member_info['profile_img_url']="";
				rencode(0,'',$member_info);
			}else{
				$startdate=strtotime($member_info['service_start_day']);
				$enddate=strtotime(date('Y-m-d',time()));
				$days=round(($enddate-$startdate)/3600/24) ;

				$insert_time=strtotime($member_info['insert_time']);
				$today=strtotime(date('Y-m-d 23:59:59',time()));
				$sign_days=round(($today-$insert_time)/3600/24) ;

				if($sign_days>=0&&$sign_days<=7){
					$member_info ['icon_flg'] = "1";//新規ユーザー(登録後7日間)
				}elseif(($days%100>=0&&$days%100<=7&&$days!=0&&$days>=100)){
					$member_info ['icon_flg'] = "2";//勤続開始100日のメンバー 
				}elseif(substr($member_info['birthday'],5,2)==date("m",time())){
					$member_info ['icon_flg'] = "3";//今月誕生日のメンバー 
				}else{
					$member_info ['icon_flg'] = "4";//最終ログイン時間の順で取得したメンバー 
				}
				if($member_info['birthday']=="0000-00-00"){
					$member_info['birthday']="";
				}
				$member_info['service_days']="$days";

				//Thanks! Level情報取得
//				$thanks_cnt = get_thanks_cnt($member_id,$shop_id);
				$thanks_cnt = $member_info['receive_thanks_cnt']+$member_info['send_thanks_cnt'];
				$setting_info_list=get_setting();

				$i=0;
				$thanks_total_cnt=$thanks_cnt;
				while($thanks_total_cnt>=0){
					//小数2桁付き
					$member_info['thanks_level_percent']=number_format(($thanks_total_cnt/$setting_info_list[$i]['setting_value']),2);
					$thanks_total_cnt=$thanks_total_cnt-$setting_info_list[$i]['setting_value'];
					$i++;
				}
				$thanks_level=$i-1;
//				$thanks_level=intval($setting_info['setting_level']);
				$member_info['thanks_level']=$thanks_level;
				 //小数2桁付き
//				$member_info['thanks_level_percent']=number_format(($thanks_cnt/$setting_info['setting_value']),2);
				//ポイント情報取得
				$member_info['point_status']=$shop_info['point_status'];//ポイント利用ステータス。0:利用中(UI表示)1:利用しない(UI表示しない)
//				$member_info['point_charge_status']=$shop_info['point_charge_status'];//ポイント付与。0:付与1:付与しない
				$member_info['point_exchange_status']=$shop_info['point_exchange_status'];//ポイント交換。0:交換1:交換しない
                //効率向上のため、holdingを独立して合算
				$get_point_holding = get_point_holding($member_id,$shop_id);
				$member_info['holding_points']=$get_point_holding['holding_points'];
				rencode(0,'',$member_info);
			}
		}
		break;
	//メールアドレス変更処理
	case 'edit_email':
		check_parameter(array($member_id,$email));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$email= html_tag_chg(SBC_DBC(trim(urldecode($email)),0));//メールアドレス
//		check_sign($_GET,$sign);

		$var = edit_email($member_id,$email);
		if($var)
		{
			rencode(0,'更新に成功しました。');
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		break;
	//パスワード変更処理
	case 'edit_pw':
		check_parameter(array($member_id,$password));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$password = trim($password);//パスワード
		$password =md5($password);
//		check_sign($_GET,$sign);

		$var = edit_pw($member_id,$password);
		if($var)
		{
			rencode(0,'更新に成功しました。');
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		break;
	//アカウント削除処理
	case 'delete_account':
		check_parameter(array($member_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
//		check_sign($_GET,$sign);

		$var = delete_account($member_id);
		$var = device_init($member_id);
		if($var){
			rencode(0,'更新に成功しました。');
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		break;
	//logout
	case 'logout':
		check_parameter(array($member_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
//		check_sign($_GET,$sign);

		$var = device_init($member_id);
		if($var){
			rencode(0,'ログアウトしました。');
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		break;
	//最終ログイン記録
	case 'upt_final_login':
		check_parameter(array($member_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
//		check_sign($_GET,$sign);

		$var = upt_final_login($member_id);
		if($var){
			rencode(0,'');
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		break;
	//サンクス通知フラグ、PUSH ON/OFF設定
	case 'set_thanks_notice_flg':
		check_parameter(array($member_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
//		check_sign($_GET,$sign);

		$member_info = get_member_info($member_id);

		if(!$member_info){
			rencode(1,'該当ユーザーが存在しません。');
		}else{
			if($member_info['thanks_notice_flg']=='0'){
				$var = set_thanks_notice_flg($member_id,'1');
			}else{
				$var = set_thanks_notice_flg($member_id,'0');
			}
			if($var){
				rencode(0,'更新に成功しました。');
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}
		break;
	//19_バッチ通知フラグ、PUSH ON/OFF設定
	case 'set_batch_notice_flg':
		check_parameter(array($member_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
//		check_sign($_GET,$sign);

		$member_info = get_member_info($member_id);
		if(!$member_info){
			rencode(1,'該当ユーザーが存在しません。');
		}else{
			if($member_info['birthday_notice_flg']=='0'){
				$var = set_batch_notice_flg($member_id,'1');
			}else{
				$var = set_batch_notice_flg($member_id,'0');
			}
			if($var){
				rencode(0,'更新に成功しました。');
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}
		break;
	//40_新規日記通知フラグ、PUSH ON/OFF設定
	case 'set_new_diary_notice_flg':
		check_parameter(array($member_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
//		check_sign($_GET,$sign);

		$member_info = get_member_info($member_id);
		if(!$member_info){
			rencode(1,'該当ユーザーが存在しません。');
		}else{
			if($member_info['diary_notice_flg']=='0'){
				$var = set_new_diary_notice_flg($member_id,'1');
			}else{
				$var = set_new_diary_notice_flg($member_id,'0');
			}
			if($var){
				rencode(0,'更新に成功しました。');
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}
		break;
}
?>
