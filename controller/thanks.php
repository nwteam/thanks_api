<?php
switch($a)
{
	//獲得Thanks!リスト取得
	case 'get_receive_thanks_list':
		check_parameter(array($member_id,$shop_id,$page));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		$page = intval($page);
		//check_sign($_GET,$sign);
		$page_number = PAGE_MAX;

		$list = get_receive_thanks_list($member_id,$shop_id,$page,$page_number);
		if ($list) {
//			$ret=update_all_thanks_read_flg($member_id,$shop_id);
			foreach ( $list as $key => $val ) {
				$ret=update_read_flg($val['thanks_id']);
			}
		}
		$total = get_receive_thanks_total_count($member_id,$shop_id);
		$l = ($total%$page_number) > 0 ? 1 : 0;
		$total_page = intval($total/$page_number) + $l;
		$cnt = get_unread_thanks_count($member_id,$shop_id);
		if($cnt!='0'){
			$arr ['unread_thanks_count'] = $cnt;
		}else{
			$arr ['unread_thanks_count'] = '0';
		}

		$unread_point_trade_info=get_unread_trade_point_cnt($member_id,$shop_id,"",2); //1:Thanks送信2:Thanks受信3:日記Thanks送信,4:日記Thanks受信,5:日記投稿6:MVP 7:予備　50:ポイント交換
		if($unread_point_trade_info==0){
			$arr['receive_point_sum_show_flg'] = "1";//表示しない
			$arr['receive_point_sum'] = "0";
			if($total!='0'){
				$arr['receive_thanks_list'] = $list;
				$arr['total_page'] = $total_page;
			}else{
				$arr['receive_thanks_list'] = array();
				$arr['total_page'] = '0';
			}
			rencode(0,'',$arr);
		}else{
			$arr['receive_point_sum_show_flg'] = "0";//表示
			$arr['receive_point_sum'] = $unread_point_trade_info;
			//読むFLG更新
			$var = set_trade_read_status($member_id,$shop_id,"",2);
			if($var){
				if($total!='0'){
					$arr['receive_thanks_list'] = $list;
					$arr['total_page'] = $total_page;
				}else{
					$arr['receive_thanks_list'] = array();
					$arr['total_page'] = '0';
				}
				rencode(0,'',$arr);
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}
		break;
	//未読Thanks!リスト取得（20レコードのみ）
	case 'get_receive_unread_thanks_list':
		check_parameter(array($member_id,$shop_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		//check_sign($_GET,$sign);

		$list = get_receive_unread_thanks_list($member_id,$shop_id);
		if ($list) {
//			$ret=update_all_thanks_read_flg($member_id,$shop_id);
			foreach ( $list as $key => $val ) {
				$ret=update_read_flg($val['thanks_id']);
			}
		}
		$cnt = get_unread_thanks_count($member_id,$shop_id);
		if($cnt!='0'){
			$arr ['unread_thanks_count'] = $cnt;
		}else{
			$arr ['unread_thanks_count'] = '0';
		}
		$unread_point_trade_info=get_unread_trade_point_cnt($member_id,$shop_id,"",2); //1:Thanks送信2:Thanks受信3:日記Thanks送信,4:日記Thanks受信,5:日記投稿6:MVP 7:予備　50:ポイント交換
		if($unread_point_trade_info==0){
			$arr['receive_point_sum_show_flg'] = "1";//表示しない
			$arr['receive_point_sum'] = "0";
			if($list){
				$arr['receive_thanks_list'] = $list;
			}else{
				$arr['receive_thanks_list'] = array();
			}
			rencode(0,'',$arr);
		}else{
			$arr['receive_point_sum_show_flg'] = "0";//表示
			$arr['receive_point_sum'] = $unread_point_trade_info;
			//読むFLG更新
			$var = set_trade_read_status($member_id,$shop_id,"",2);
			if($var){
				if($list){
					$arr['receive_thanks_list'] = $list;
				}else{
					$arr['receive_thanks_list'] = array();
				}
				rencode(0,'',$arr);
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}
		break;
	//送信Thanks!リスト取得
	case 'get_send_thanks_list':
		check_parameter(array($member_id,$page));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$page = intval($page);
		//check_sign($_GET,$sign);
		$page_number = PAGE_MAX;

		$list = get_send_thanks_list($member_id,$shop_id,$page,$page_number);

		$total = get_send_thanks_total_count($member_id,$shop_id);
		$l = ($total%$page_number) > 0 ? 1 : 0;
		$total_page = intval($total/$page_number) + $l;

//		$arr['send_thanks_list'] = $list;
//		$arr['total_page'] = $total_page;

		if($total!='0'){
			$arr['send_thanks_list'] = $list;
			$arr['total_page'] = $total_page;
		}else{
			$arr['receive_thanks_list'] = array();
			$arr['total_page'] = '0';
		}
		rencode(0,'',$arr);
		break;
	//メンバー一覧取得
	case 'get_staff_list':
		check_parameter(array($shop_id,$member_id));
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		//check_sign($_GET,$sign);

		$tmp1 = array ();
//		$tmp2 = array ();
		$tmp = array ();
//		//勤続開始100日のメンバー
//		$list1 = get_service_100days_staff_list($shop_id);
//		if ($list1) {
//			foreach ( $list1 as $key => $val ) {
//				$tmp1 [$key] ['member_id'] = $val ['member_id'];
//				$tmp1 [$key] ['member_name'] = $val ['member_name'];
//				$tmp1 [$key] ['profile_img_url'] = $val ['profile_img_url'];
//				$tmp1 [$key] ['icon_flg'] = "1";//勤続開始100日のメンバー
//				$startdate=strtotime($val['service_start_day']);
//				$enddate=strtotime(date('Y-m-d',time()));
//				$days=round(($enddate-$startdate)/3600/24) ;
//				$tmp1 [$key] ['service_days']="$days";
//			}
//		}
//		//今月誕生日のメンバー
//		$list2 = get_birthday_staff_list($shop_id);
//		if ($list2) {
//			foreach ( $list2 as $key => $val ) {
//				$tmp2 [$key] ['member_id'] = $val ['member_id'];
//				$tmp2 [$key] ['member_name'] = $val ['member_name'];
//				$tmp2 [$key] ['profile_img_url'] = $val ['profile_img_url'];
//				$tmp2 [$key] ['icon_flg'] = "2";//今月誕生日のメンバー
//				$startdate=strtotime($val['service_start_day']);
//				$enddate=strtotime(date('Y-m-d',time()));
//				$days=round(($enddate-$startdate)/3600/24) ;
//				$tmp1 [$key] ['service_days']="$days";
//			}
//		}
		//最終ログイン時間の順で取得したメンバー
		$list = get_nomal_staff_list($shop_id,$member_id);
		if ($list) {
			foreach ( $list as $key => $val ) {
				$startdate=strtotime($val['service_start_day']);
				$enddate=strtotime(date('Y-m-d',time()));
				$days=round(($enddate-$startdate)/3600/24) ;
//
//				$startdate2=strtotime($val['service_start_day']);
//				$enddate2=strtotime('-7 days');
//				$days2=round(($enddate2-$startdate2)/3600/24) ;

				$insert_time=strtotime($val['insert_time']);
				$today=strtotime(date('Y-m-d 23:59:59',time()));
				$sign_days=round(($today-$insert_time)/3600/24) ;

				if($sign_days>=0&&$sign_days<=7){
					$tmp [$key] ['icon_flg'] = "1";//新規ユーザー(登録後7日間)
				}elseif(($days%100>=0&&$days%100<=7&&$days!=0&&$days>=100)){
					$tmp [$key] ['icon_flg'] = "2";//勤続開始100日のメンバー
				}elseif(substr($val['birthday'],5,2)==date("m",time())){
					$tmp [$key] ['icon_flg'] = "3";//今月誕生日のメンバー
				}else{
					$tmp [$key] ['icon_flg'] = "4";//最終ログイン時間の順で取得したメンバー
				}
				$tmp [$key] ['member_name'] = $val ['member_name'];
				$tmp [$key] ['final_login_time'] = $val ['final_login_time'];
				$tmp [$key] ['service_days']="$days";
				$tmp [$key] ['sign_days']="$sign_days";
				$tmp [$key] ['member_id'] = $val ['member_id'];
				$tmp [$key] ['insert_time'] = $val ['insert_time'];
				$tmp [$key] ['birthday'] = $val ['birthday'];
				$tmp [$key] ['service_start_day'] = $val ['service_start_day'];
				$tmp [$key] ['profile_img_url'] = $val ['profile_img_url'];
			}
			$tmp1 [0]['icon_flg'] = "0";//全員に送信
			$tmp1 [0]['member_name'] = "メンバー全員";
			$tmp1 [0]['final_login_time'] = "";
			$tmp1 [0]['service_days']="";
			$tmp1 [0]['sign_days']="";
			$tmp1 [0]['member_id'] = "";
			$tmp1 [0]['insert_time'] = "";
			$tmp1 [0]['birthday'] = "";
			$tmp1 [0]['service_start_day'] = "";
			$shop_info = get_shop_info($shop_id);
			$agency_id = $shop_info['agency_id'];//代理店ID
			if ($agency_id == "") {
				$agency_id = 0;
			}
			$chain_id = $shop_info['chain_id'];//加盟店ID
			if ($chain_id == "") {
				$chain_id = 0;
			}
			$get_theme_info = get_theme_info($shop_id, $agency_id, $chain_id);
			if($get_theme_info) {
				$tmp1 [0]['profile_img_url'] = $get_theme_info['icon_send_to_all_avator_img_url'];
			}else{
				$tmp1 [0]['profile_img_url'] = "upload/all_user.png";
			}
		}
//		$arr['staff_list'] = array_merge($list1,$list2,$list3);
		$re_arr=array_merge($tmp1,$tmp);
		array_multisort($re_arr);
		$arr['staff_list']=$re_arr;
		if($arr){
			rencode(0,'',$arr);
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		break;
	//Thanks!送信処理
	case 'send_thanks':
		check_parameter(array($member_id,$target_id,$stamp_id,$shop_id));
		$member_id= intval(html_tag_chg($member_id));//FromユーザーID
		$target_id= intval(html_tag_chg($target_id));//宛先ユーザーID
		$stamp_id= intval(html_tag_chg($stamp_id));//スタンプID
		$shop_id= intval(html_tag_chg($shop_id));//チームID

		$trade_year = date("Y");
		$trade_month = date("m");
		$trade_day = date("d");

		if($thanks_msg!=''){
			$thanks_msg= html_tag_chg(SBC_DBC(trim(urldecode($thanks_msg)),0));//Thanks!メッセージ
			//$thanks_msg_DB= preg_replace('/[\xF0-\xF7][\x80-\xBF][\x80-\xBF][\x80-\xBF]/','',$thanks_msg);//Thanks!メッセージDB格納用（4バイト絵文字取り除く）
			$thanks_msg_DB=$thanks_msg;
		}else{
			$thanks_msg="";
			$thanks_msg_DB="";
		}
		//check_sign($_GET,$sign);
		$member_info = get_shop_member_info($target_id,$shop_id);
		$shop_info = get_shop_info($shop_id);

		$login_member_info = get_member_info($member_id);
		$login_shop_info = get_shop_info($login_member_info['shop_id']);

        $point_status=$shop_info['point_status'];//ポイント利用ステータス。0:利用中(UI表示)1:利用しない(UI表示しない)
        $point_charge_status=$shop_info['point_charge_status'];//ポイント付与。0:付与1:付与しない
        $point_thanks_send=$shop_info['point_thanks_send']; //サンクス送信獲得ポイント
		$point_thanks_receive=$shop_info['point_thanks_receive']; //サンクス受信獲得ポイント

		if($login_member_info['status']=="2"&&$login_shop_info['status']=="0"&&$member_info['status']=="2"&&$shop_info['status']=="0"&&$member_info['member_id']!=$member_id) {
			$var = send_thanks($target_id, $stamp_id, $thanks_msg_DB, $member_id,$login_member_info['shop_id'],$point_status,$point_charge_status,$point_thanks_send,$point_thanks_receive);
			$thanks_id=$var;
			if($var){
				if($member_info){
					if($member_info['device_type'] == "1") {
						if ($member_info['thanks_notice_flg'] == "0"&&$member_info['device_id']!="") {
//							$badge = get_unread_thanks_count($target_id,$shop_id);
							$badge = get_unread_count($target_id,$shop_id);
							if ($thanks_msg == '') {
								$thanks_msg = "Thanks!スタンプが届きました！";
							}
//							$var = send_push_ios($thanks_msg, $member_info['device_id'], $badge);
							$var = send_push_ios_with_type($thanks_msg, $member_info['device_id'], $badge,"3");//獲得Thanks!一覧に遷移
						}
					}else {
						if ($member_info['thanks_notice_flg'] == "0"&&$member_info['device_id']!="") {
							$registatoin_ids = array($member_info['device_id']);
							if ($thanks_msg == '') {
								$thanks_msg = "Thanks!スタンプが届きました！";
							}
//							$thanks_msg = array("msg" => $thanks_msg);
//							$var = send_push_android($thanks_msg, $registatoin_ids);
							$var = send_push_android_with_type($thanks_msg, $registatoin_ids,"3");//獲得Thanks!一覧に遷移
						}
					}
				}
				$unread_point_trade_info=get_unread_trade_point_cnt($member_id,$shop_id,"",1); //1:Thanks送信2:Thanks受信3:日記Thanks送信,4:日記Thanks受信,5:日記投稿6:MVP 7:予備　50:ポイント交換
				if($unread_point_trade_info=="0") {
					$arr['receive_point_sum_show_flg'] = "1";//表示しない
					$arr['receive_point_sum'] = "0";
//					rencode(0,'Thanks!を送信しました。',$arr);
				}else{
					$arr['receive_point_sum_show_flg'] = "0";//表示
					$arr['receive_point_sum'] = $unread_point_trade_info;
					set_trade_read_status($member_id,$shop_id,"",1);
//					$re_msg='Thanks!を送信しました。'."\r\n".$unread_point_trade_info.'ポイントを獲得しました。';
//					rencode(0,$re_msg,$arr);
				}
                rencode(0,'Thanks!を送信しました。',$arr);
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}elseif($member_info['bot_flg']=='1'){
			rencode(1,'当該ユーザに送信できません。');
		}else{
			if($login_shop_info['status']!="0"||$shop_info['status']!="0"){
				rencode(1,'現在このチームのThanks!は利用停止中です。');
			}else{
				rencode(1,'このユーザーは退会済みです。');
			}
		}
		break;
	//20_Thanks!スタンプ情報取得
	case 'get_thanks_stamp_list':
		//check_sign($_GET,$sign);

//		$thanks_stamp_info = get_thanks_stamp_list();
//		if(!$thanks_stamp_info){
//			rencode(1,'該当ユーザーが存在しません。');
//		}else{
//			rencode(0,'',$thanks_stamp_info);
//		}
		check_parameter(array($member_id,$shop_id));
		$member_id= intval(html_tag_chg($member_id));//FromユーザーID
		$shop_id= intval(html_tag_chg($shop_id));//チームID

		$cate_arr = array ();
		$cate_arrs = array ();
		$stamp_arr = array ();
		$stamp_arrs = array ();
		$shop_info = get_shop_info($shop_id);
		$agency_id=$shop_info['agency_id'];//代理店ID
		if($agency_id==""){$agency_id=0;}
		$chain_id=$shop_info['chain_id'];//加盟店ID
		if($chain_id==""){$chain_id=0;}
		$thanks_stamp_category_list = get_thanks_stamp_category_list();
		if ($thanks_stamp_category_list) {
			$i = 0;
			foreach ($thanks_stamp_category_list as $category_key => $category_val) {
				$thanks_stamp_list=get_thanks_stamp_list($category_val['category_id'],$member_id,$shop_id,$agency_id,$chain_id);
				if($thanks_stamp_list){
					$cate_arr [$i] = $category_val['category_name'];
//					$stamp_arr[$i] = get_thanks_stamp_list($category_val['category_id'],$member_id,$shop_id);
					$stamp_arr[$i] = $thanks_stamp_list;
//					$cate_arr [$i] = $thanks_stamp_list;
					$i++;
				}
			}
		}
		$cate_arrs['category_name']=$cate_arr;
		$stamp_arrs['category_list']=$stamp_arr;
		$merge_arr=array_merge($cate_arrs,$stamp_arrs);
		rencode(0,'',$merge_arr);
		break;

	//21_ランキング情報取得
	case 'get_ranking_list':
		//check_sign($_GET,$sign);
		check_parameter(array($year,$month,$shop_id));
		$shop_id= intval(html_tag_chg($shop_id));//チームID

		$ranking_info = get_ranking_list($year,$month,$shop_id);
		if(!$ranking_info){
			rencode(1,'データが存在しません。');
		}else{
			rencode(0,'',$ranking_info);
		}
		break;
	//未読Thanks!数取得
	case 'get_unread_thanks_count':
		check_parameter(array($member_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		//check_sign($_GET,$sign);

		$cnt = get_unread_thanks_count($member_id,$shop_id);

		if($cnt!='0'){
			$arr['unread_thanks_count'] = $cnt;
		}else{
			$arr['unread_thanks_count'] = '0';
		}
		rencode(0,'',$arr);
		break;
	//全員に送信
	case 'send_thanks_to_all':
		check_parameter(array($member_id,$shop_id,$stamp_id));
		$member_id= intval(html_tag_chg($member_id));//FromユーザーID
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		$stamp_id= intval(html_tag_chg($stamp_id));//スタンプID
		if($thanks_msg!=''){
			$thanks_msg= html_tag_chg(SBC_DBC(trim(urldecode($thanks_msg)),0));//Thanks!メッセージ
			//$thanks_msg_DB= preg_replace('/[\xF0-\xF7][\x80-\xBF][\x80-\xBF][\x80-\xBF]/','',$thanks_msg);//Thanks!メッセージDB格納用（4バイト絵文字取り除く）
			$thanks_msg_DB=$thanks_msg;
		}else{
			$thanks_msg="";
			$thanks_msg_DB="";
		}
		//check_sign($_GET,$sign);
//		$member_array = get_member_array($shop_id,$member_id);
		$member_array = get_shop_member_list($shop_id);
		$shop_info = get_shop_info($shop_id);
		$login_member_info = get_member_info($member_id);
		$login_shop_info = get_shop_info($login_member_info['shop_id']);

		$point_status=$shop_info['point_status'];//ポイント利用ステータス。0:利用中(UI表示)1:利用しない(UI表示しない)
		$point_charge_status=$shop_info['point_charge_status'];//ポイント付与。0:付与1:付与しない
		$point_thanks_send=$shop_info['point_thanks_send']; //サンクス送信獲得ポイント
		$point_thanks_receive=$shop_info['point_thanks_receive']; //サンクス受信獲得ポイント

//		print_r($member_array);die;
		$ret="0";
		if ($member_array) {
			if($login_member_info['status'] == "2" && $login_shop_info['status'] == "0") {
				foreach ($member_array as $key => $member_info) {
					if ($login_member_info['status'] == "2" && $login_shop_info['status'] == "0" && $member_info['status'] == "2" && $shop_info['status'] == "0" && $member_info['member_id'] != $member_id) {
						$var = send_thanks($member_info['member_id'], $stamp_id, $thanks_msg_DB, $member_id, $login_member_info['shop_id'],$point_status,$point_charge_status,$point_thanks_send,$point_thanks_receive);
						if ($var) {
//							if ($member_info) {
//								if ($member_info['device_type'] == "1") {
//									if ($member_info['thanks_notice_flg'] == "0" && $member_info['device_id'] != "") {
//										//									$badge = get_unread_thanks_count($member_info['member_id'],$member_info['shop_id']);
//										$badge = get_unread_count($member_info['member_id'], $member_info['shop_id']);
//										if ($thanks_msg == '') {
//											$thanks_msg = "Thanks!スタンプが届きました！";
//										}
////										$var = send_push_ios($thanks_msg, $member_info['device_id'], $badge);
//										$var = send_push_ios_with_type($thanks_msg, $member_info['device_id'], $badge,"3");//獲得Thanks!一覧に遷移
//									}
//								} else {
//									if ($member_info['thanks_notice_flg'] == "0" && $member_info['device_id'] != "") {
//										$registatoin_ids = array($member_info['device_id']);
//										if ($thanks_msg == '') {
//											$thanks_msg = "Thanks!スタンプが届きました！";
//										}
////										$thanks_msg = array("msg" => $thanks_msg);
////										$var = send_push_android($thanks_msg, $registatoin_ids);
//										$var = send_push_android_with_type($thanks_msg, $registatoin_ids,"3");//獲得Thanks!一覧に遷移
//									}
//								}
//							}
							//						rencode(0, 'Thanks!を送信しました。');
						}else{
							$ret="1";
						}
						//					else {
						//						rencode(1, 'エラーが発生しました。しばらく待ってから再度試してみてください。');
						//					}
					}
				}
			}
			else {
				if ($login_shop_info['status'] != "0" || $shop_info['status'] != "0") {
					rencode(1, '現在このチームのThanks!は利用停止中です。');
				}
				else {
					rencode(1, 'このユーザーは退会済みです。');
				}
			}
			if ($ret=="0") {
				$exec_file = DOCUMENT_ROOT .VERSION_PATH.'common/thread_push_thanks_to_all.php';
				exec("nohup php -c '' $exec_file $member_id $shop_id $stamp_id $thanks_msg  > /dev/null &");
			}
		}
		$unread_point_trade_info=get_unread_trade_point_cnt($member_id,$shop_id,"",1); //1:Thanks送信2:Thanks受信3:日記Thanks送信,4:日記Thanks受信,5:日記投稿6:MVP 7:予備　50:ポイント交換
		if($unread_point_trade_info==0) {
			$arr['receive_point_sum_show_flg'] = "1";//表示しない
			$arr['receive_point_sum'] = "0";
//			rencode(0,'Thanks!を送信しました。',$arr);
		}else{
			$arr['receive_point_sum_show_flg'] = "0";//表示
			$arr['receive_point_sum'] = $unread_point_trade_info;
			set_trade_read_status($member_id,$shop_id,"",1);
//			$re_msg='Thanks!を送信しました。'."\r\n".$unread_point_trade_info.'ポイントを獲得しました。';
//			rencode(0,$re_msg,$arr);
		}
		rencode(0,'Thanks!を送信しました。',$arr);
		break;
	//サンクス返信フラグ設定
	case 'set_reply_flg':
		check_parameter(array($thanks_id));
		$thanks_id= intval(html_tag_chg($thanks_id));//返信対応ThanksID

		$thanks_info = get_thanks_info($thanks_id);

		if(!$thanks_info){
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}else{
			$var = set_reply_flg($thanks_id,'1');
			if($var){
				rencode(0,'');
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}
		break;
}
?>