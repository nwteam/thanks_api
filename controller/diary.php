<?php
switch($a)
{
	//日記投稿
	case 'new_diary':
		check_parameter(array($member_id,$shop_id,$stamp_id,$diary_contents));
		$member_id= intval(html_tag_chg($member_id));//日記作成者ID
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		$stamp_id= intval(html_tag_chg($stamp_id));//スタンプID
		$img_url= html_tag_chg($img_url);//画像URL
		if($diary_contents!=''){
			$diary_contents= html_tag_chg(SBC_DBC(trim(urldecode($diary_contents)),0));//日記内容
		}else{
			$diary_contents="";
		}
		//check_sign($_GET,$sign);
		$login_member_info = get_shop_member_info($member_id,$shop_id);
		$login_shop_info = get_shop_info($shop_id);
		if($login_member_info['status']=="2"&&$login_shop_info['status']=="0") {
			//ダブル投稿防ぐ
			$same_diary_cnt=get_same_diary_cnt($member_id,$login_member_info['shop_id'],$stamp_id,$diary_contents,$img_url);
			if($same_diary_cnt!=0){
				rencode(0, '');
			}else{
				$var = create_diary($member_id,$login_member_info['shop_id'],$stamp_id,$diary_contents,$img_url);
				$diary_id=$var;
//	//			$member_array = get_member_array($shop_id,$member_id);
//				$member_array = get_shop_member_list($shop_id);
//				$shop_info = get_shop_info($shop_id);
//				if ($member_array) {
//					foreach ($member_array as $key => $target_member_info) {
//						if ($login_member_info['status'] == "2" && $target_member_info['status'] == "2" && $shop_info['status'] == "0"&&$target_member_info['diary_notice_flg'] == "0") {
//							$notice_msg="日記に新しい投稿がありました。皆でThanks!を贈りましょう！";
//							$notice_msg= html_tag_chg(SBC_DBC(trim(urldecode($notice_msg)),0));//Noticeメッセージ
//							if($var){
//								if($target_member_info['member_id']!=$member_id){
//									save_diary_notice_info($diary_id,$target_member_info['member_id'],$notice_msg,$member_id,$shop_id);
//									send_push_notice($target_member_info,$notice_msg,"1",$target_member_info['diary_notice_flg']);//お知らせ一覧に遷移
//								}
//							}
//						}
//					}
//				}
				if ($var) {
					//チームの皆にPush通知
					$exec_file=DOCUMENT_ROOT.VERSION_PATH.'common/thread_push_new_diary.php';
					exec("nohup php -c '' $exec_file $member_id $shop_id $diary_id  > /dev/null &");
					rencode(0, '日記を作成しました。');
				}else {
					rencode(1, 'エラーが発生しました。しばらく待ってから再度試してみてください。');
				}
			}
		}else{
			if($login_shop_info['status']!="0"){
				rencode(1,'現在このチームのThanks!は利用停止中です。');
			}else{
				rencode(1,'このユーザーは退会済みです。');
			}
		}
		break;
	//日記一覧取得
	case 'get_diary_list':
		check_parameter(array($get_flg,$login_member_id));
		if($get_flg=="1"){//日記IDのみ指定する場合、該当日記情報を取得
			check_parameter(array($diary_id));
			$diary_id= intval(html_tag_chg($diary_id));//日記ID
		}elseif($get_flg=="2"){//チームID＋会员ID指定する場合、会員単位で日記情報を取得(マイページ、プロフ用)20件限定
			check_parameter(array($shop_id,$member_id));
			$shop_id= intval(html_tag_chg($shop_id));//チームID
			$member_id= intval(html_tag_chg($member_id));//ユーザーID
		}elseif($get_flg=="3"){//チームID＋会员ID指定する場合、会員単位で日記情報を取得（日記一覧　More日記に使う）
			check_parameter(array($shop_id,$member_id,$page));
			$shop_id= intval(html_tag_chg($shop_id));//チームID
			$member_id= intval(html_tag_chg($member_id));//ユーザーID
			$page = intval($page);
			$page_number = PAGE_MAX;
		}elseif($get_flg=="0"){//チームIDのみ指定する場合、チーム単位で日記情報を取得
			check_parameter(array($shop_id,$page));
			$shop_id= intval(html_tag_chg($shop_id));//チームID
			$page = intval($page);
			$page_number = PAGE_MAX;
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		//check_sign($_GET,$sign);
		$tmp = array ();
		$total='0';
		$total_page='0';
		//チーム単位で日記一覧取得
		if($get_flg=="1"){//日記IDのみ指定する場合、該当日記情報を取得
			$list = get_diary_list_by_id($diary_id);
		}elseif($get_flg=="2"){//チームID＋会员ID指定する場合、会員単位で日記情報を取得(マイページ、プロフ用)20件限定
			$list = get_diary_list_by_member_limited($shop_id,$member_id);
			$total = get_menber_diary_total_count($shop_id,$member_id);
		}elseif($get_flg=="3"){//チームID＋会员ID指定する場合、会員単位で日記情報を取得（日記一覧　More日記に使う）
			$list = get_diary_list_by_member_more($shop_id,$member_id,$page,$page_number);
			$total = get_menber_diary_total_count($shop_id,$member_id);
			$l = ($total%$page_number) > 0 ? 1 : 0;
			$total_page = intval($total/$page_number) + $l;
		}elseif($get_flg=="0"){//チームIDのみ指定する場合、チーム単位で日記情報を取得
			$list = get_diary_list_by_shop($shop_id,$page,$page_number);
			$total = get_shop_diary_total_count($shop_id);
			$l = ($total%$page_number) > 0 ? 1 : 0;
			$total_page = intval($total/$page_number) + $l;
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}

		if ($list) {
			$i=0;
			foreach ( $list as $key => $val ) {
					$member_info = get_shop_member_info($val ['member_id'], $val ['shop_id']);
					$stamp_info = get_stamp_info($val ['stamp_id']);
					$my_thanks_cnt = get_my_thanks_cnt($val ['diary_id'], $login_member_id);//該当日記に自己Thanks!送信数を取得
					$thanks_cnt = $val ['thanks_cnt'];
				if($member_info['status']=='2'||$member_info['bot_flg']=='1') {
					$tmp [$i] ['member_name'] = $member_info ['member_name'];
					$tmp [$i] ['profile_img_url'] = $member_info ['profile_img_url'];
					$tmp [$i] ['diary_id'] = $val ['diary_id'];
					$tmp [$i] ['member_id'] = $val ['member_id'];
					$tmp [$i] ['shop_id'] = $val ['shop_id'];
					$tmp [$i] ['stamp_id'] = $val ['stamp_id'];
					$tmp [$i] ['stamp_image_url'] = $stamp_info ['image_url'];
					$tmp [$i] ['diary_contents'] = $val ['diary_contents'];
					$tmp [$i] ['img_url'] = $val ['img_url'];
					$tmp [$i] ['thanks_cnt'] = $thanks_cnt;
					if ($my_thanks_cnt == "0") {
						$tmp [$i] ['thanks_cnt_color_flg'] = "0";
					} else {
						if ($thanks_cnt > 0 && $thanks_cnt < 10) {
							$tmp [$i] ['thanks_cnt_color_flg'] = "1";
						} elseif ($thanks_cnt > 9 && $thanks_cnt < 20) {
							$tmp [$i] ['thanks_cnt_color_flg'] = "2";
						} elseif ($thanks_cnt > 19 && $thanks_cnt < 30) {
							$tmp [$i] ['thanks_cnt_color_flg'] = "3";
						} elseif ($thanks_cnt > 29 && $thanks_cnt < 40) {
							$tmp [$i] ['thanks_cnt_color_flg'] = "4";
						} elseif ($thanks_cnt > 39 && $thanks_cnt < 50) {
							$tmp [$i] ['thanks_cnt_color_flg'] = "5";
						} elseif ($thanks_cnt > 49 && $thanks_cnt < 60) {
							$tmp [$i] ['thanks_cnt_color_flg'] = "6";
						} elseif ($thanks_cnt > 59 && $thanks_cnt < 70) {
							$tmp [$i] ['thanks_cnt_color_flg'] = "7";
						} else {
							$tmp [$i] ['thanks_cnt_color_flg'] = "8";
						}
					}
					$tmp [$i] ['insert_time'] = $val ['insert_time'];
					$tmp [$i] ['update_time'] = $val ['update_time'];
					$i++;
				}else{
					continue;
				}
			}
		}

//		$re_arr=array_merge($tmp1,$tmp);
//		array_multisort($re_arr);
//		$arr['diary_list']=$tmp;
		if($total!='0'&&($get_flg=="0"||$get_flg=="3")){
			$arr['diary_list']=$tmp;
			$arr['total_page'] = $total_page;
		}else{
			$arr['diary_list'] = $tmp;
			$arr['total_page'] = '1';
		}
		//すべてのAPIにmore_flgを返すが、マイページとプロフのみで使う。他は無視。
		$arr['more_flg'] = ($total>20) ? '1' : '0' ;
		if($arr){
			rencode(0,'',$arr);
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		break;

	//Thanks！した人一覧情報取得
	case 'get_diary_thanks_list':
		check_parameter(array($diary_id,$login_member_id));
		$login_member_id= intval(html_tag_chg($login_member_id));//ログインユーザーID
		$diary_id= intval(html_tag_chg($diary_id));//日記ID
		//check_sign($_GET,$sign);
		$tmp = array ();

		$member_info = get_member_info($login_member_id);
		$shop_id = $member_info['shop_id'];
		$diary_info=get_diary_info($diary_id);
		$diary_owner_id=$diary_info['member_id'];
		$list = get_diary_thanks_list($diary_id);

		if ($list) {
			foreach ( $list as $key => $val ) {
//				$target_member_info = get_shop_member_info($val ['target_id'],$val ['shop_id']);
				$from_member_info = get_shop_member_info($val ['from_id'],$val ['shop_id']);
				$stamp_info = get_stamp_info( $val ['stamp_id']);
				$my_thanks_cnt=get_my_thanks_cnt($val ['diary_id'],$login_member_id);//該当日記に自己Thanks!送信数を取得
//				$thanks_cnt = $val ['thanks_cnt'];
				if($from_member_info['status']!='2'&&($from_member_info['status']=='9'&&$from_member_info['bot_flg']!='1')){
					$tmp [$key] ['member_name'] ="退会済みユーザー";
					$tmp [$key] ['profile_img_url'] ="";
				}else{
					$tmp [$key] ['member_name'] = $from_member_info ['member_name'];
					$tmp [$key] ['profile_img_url'] = $from_member_info ['profile_img_url'];
				}
				$tmp [$key] ['diary_id'] = $val ['diary_id'];
				$tmp [$key] ['target_id'] = $val ['target_id'];
				$tmp [$key] ['from_id'] = $val ['from_id'];
				$tmp [$key] ['shop_id'] = $val ['shop_id'];
				$tmp [$key] ['stamp_id'] = $val ['stamp_id'];
				$tmp [$key] ['stamp_image_url'] = $stamp_info ['image_url'];
				$tmp [$key] ['thanks_msg'] = $val ['thanks_msg'];

				if($my_thanks_cnt=="0"){
					$tmp [$key] ['thanks_cnt_color_flg']="0";//グレー
				}else{
					$tmp [$key] ['thanks_cnt_color_flg']="1";//緑
				}
				$tmp [$key] ['send_time'] = $val ['send_time'];
			}
		}

//		$re_arr=array_merge($tmp1,$tmp);
//		array_multisort($re_arr);
		$arr['diary_thanks_list']=$tmp;
		$unread_point_trade_info=get_unread_trade_point_cnt($login_member_id,$shop_id,$diary_id,4); //1:Thanks送信2:Thanks受信3:日記Thanks送信,4:日記Thanks受信,5:日記投稿6:MVP 7:予備　50:ポイント交換
		if($unread_point_trade_info==0){
			$arr['receive_point_sum_show_flg'] = "1";//表示しない
			$arr['receive_point_sum'] = "0";
			if($arr){
				rencode(0,'',$arr);
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}elseif($unread_point_trade_info!=0&&$login_member_id!=$diary_owner_id) {
			$arr['receive_point_sum_show_flg'] = "1";//表示しない
			$arr['receive_point_sum'] = "0";
			if($arr){
				rencode(0,'',$arr);
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}else{
			$arr['receive_point_sum_show_flg'] = "0";//表示
			$arr['receive_point_sum'] = $unread_point_trade_info;
			//読むFLG更新
			$var = set_trade_read_status($login_member_id,$shop_id,$diary_id,4);
			if($var){
				if($arr){
					rencode(0,'',$arr);
				}else{
					rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
				}
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}
		break;

	//日記にThanks!送信処理
	case 'send_diary_thanks':
		check_parameter(array($diary_id,$member_id,$target_id,$stamp_id,$shop_id));
		$diary_id= intval(html_tag_chg($diary_id));//日記ID
		$member_id= intval(html_tag_chg($member_id));//FromユーザーID
		$target_id= intval(html_tag_chg($target_id));//宛先ユーザーID
		$stamp_id= intval(html_tag_chg($stamp_id));//スタンプID
		$shop_id= intval(html_tag_chg($shop_id));//チームID

		$trade_year = date("Y");
		$trade_month = date("m");
		$trade_day = date("d");

		if($thanks_msg!=''){
			$thanks_msg= html_tag_chg(SBC_DBC(trim(urldecode($thanks_msg)),0));//Thanks!メッセージ
		}else{
			$thanks_msg="";
		}

		//check_sign($_GET,$sign);
		$member_info = get_shop_member_info($target_id,$shop_id);
		$shop_info = get_shop_info($shop_id);
		$login_member_info = get_member_info($member_id);
//		$login_shop_info = get_shop_info($login_member_info['shop_id']);

		$point_status=$shop_info['point_status'];//ポイント利用ステータス。0:利用中(UI表示)1:利用しない(UI表示しない)
		$point_charge_status=$shop_info['point_charge_status'];//ポイント付与。0:付与1:付与しない
		$point_thanks_send=$shop_info['point_thanks_send']; //サンクス送信獲得ポイント
		$point_thanks_receive=$shop_info['point_thanks_receive']; //サンクス受信獲得ポイント

		if($login_member_info['status']=="2"&&($member_info['status']=="2"||$member_info['bot_flg']=='1')&&$shop_info['status']=="0") {
			$var = send_diary_thanks($diary_id,$target_id, $stamp_id, $thanks_msg, $member_id,$shop_id,$point_status,$point_charge_status,$point_thanks_send,$point_thanks_receive);
			$notice_msg=$login_member_info['member_name']."さんがあなたの投稿にThanks!を贈りました。";
			$notice_msg= html_tag_chg(SBC_DBC(trim(urldecode($notice_msg)),0));//Noticeメッセージ
			if($var){
				if($member_info) {
					if ($member_info['member_id'] != $member_id) {
						save_diary_notice_info($diary_id, $target_id, $notice_msg, $member_id, $shop_id);
						send_push_notice($member_info,$notice_msg,"1",$member_info['thanks_notice_flg']);//お知らせ一覧に遷移
//						if ($member_info['device_type'] == "1") {
//							if ($member_info['thanks_notice_flg'] == "0" && $member_info['device_id'] != "") {
//								$badge = get_unread_count($member_id, $shop_id);
////								$var = send_push_ios($notice_msg, $member_info['device_id'], $badge);
//								$var = send_push_ios_with_type($notice_msg, $member_info['device_id'], $badge,"1");//お知らせ一覧に遷移
//							}
//						} else {
//							if ($member_info['thanks_notice_flg'] == "0" && $member_info['device_id'] != "") {
//								$registatoin_ids = array($member_info['device_id']);
////								$notice_msg = array("msg" => $notice_msg);
////								$var = send_push_android($notice_msg, $registatoin_ids);
//								$var = send_push_android_with_type($notice_msg, $registatoin_ids,"1");//お知らせ一覧に遷移
//							}
//						}
					}
				}
				$unread_point_trade_info=get_unread_trade_point_cnt($member_id,$shop_id,"",3); //1:Thanks送信2:Thanks受信3:日記Thanks送信,4:日記Thanks受信,5:日記投稿6:MVP 7:予備　50:ポイント交換
				if($unread_point_trade_info==0) {
					$arr['receive_point_sum_show_flg'] = "1";//表示しない
					$arr['receive_point_sum'] = "0";
//					rencode(0,'Thanks!を贈りました。',$arr);
				}else{
					$arr['receive_point_sum_show_flg'] = "0";//表示
					$arr['receive_point_sum'] = $unread_point_trade_info;
					set_trade_read_status($member_id,$shop_id,"",3);
//					$re_msg='Thanks!を贈りました。'."\r\n".$unread_point_trade_info.'ポイントを獲得しました。';
//					rencode(0,$re_msg,$arr);
				}
				rencode(0,'Thanks!を贈りました。',$arr);
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}else{
			if($login_shop_info['status']!="0"||$shop_info['status']!="0"){
				rencode(1,'現在このチームのThanks!は利用停止中です。');
			}else{
				rencode(1,'このユーザーは退会済みです。');
			}
		}
		break;
	//日記編集
	case 'edit_diary':
		check_parameter(array($member_id,$shop_id,$stamp_id,$diary_id,$diary_contents));
		$member_id= intval(html_tag_chg($member_id));//日記作成者ID
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		$stamp_id= intval(html_tag_chg($stamp_id));//スタンプID
		$img_url= html_tag_chg($img_url);//画像URL
		if($diary_contents!=''){
			$diary_contents= html_tag_chg(SBC_DBC(trim(urldecode($diary_contents)),0));//日記内容
		}else{
			$diary_contents="";
		}
		//check_sign($_GET,$sign);
		$login_member_info = get_shop_member_info($member_id,$shop_id);
		$login_shop_info = get_shop_info($shop_id);
		if($login_member_info['status']=="2"&&$login_shop_info['status']=="0") {
			$var = update_diary_info($diary_id,$stamp_id,$diary_contents,$img_url);
			if ($var) {
				rencode(0, '日記を更新しました。');
			}else {
				rencode(1, 'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}else{
			if($login_shop_info['status']!="0"){
				rencode(1,'現在このチームのThanks!は利用停止中です。');
			}else{
				rencode(1,'このユーザーは退会済みです。');
			}
		}
		break;
	//日記削除
	case 'delete_diary':
		check_parameter(array($member_id,$diary_id));
		$member_id= intval(html_tag_chg($member_id));//日記作成者ID

		//check_sign($_GET,$sign);
		$login_member_info = get_member_info($member_id);
		$login_shop_info = get_shop_info($login_member_info['shop_id']);
		if($login_member_info['status']=="2"&&$login_shop_info['status']=="0") {
			$var = delete_diary_info($diary_id);
			if ($var) {
				rencode(0, '日記を削除しました。');
			}else {
				rencode(1, 'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}else{
			if($login_shop_info['status']!="0"){
				rencode(1,'現在このチームのThanks!は利用停止中です。');
			}else{
				rencode(1,'このユーザーは退会済みです。');
			}
		}
		break;
}
?>