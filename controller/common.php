<?php
switch($a)
{
	//強制更新チェック
	case 'version_chk':
		check_parameter(array($os,$version));
		//check_sign($_GET,$sign);
		if($os=="1"){
			$online_version = get_online_version("1",$version);//iOS
		}else{
			$online_version = get_online_version("2",$version);//Android
		}

//		if($online_version!=$version) {
		if($online_version<=$version) {
			rencode(0, '');
		}else{
			rencode(1, '');
		}
		break;

	//Thanks!カスタマイズ情報取得
	case 'get_theme_info':
		check_parameter(array($shop_id));
		$shop_id = intval(html_tag_chg($shop_id));//チームID

		$shop_info = get_shop_info($shop_id);
		$agency_id = $shop_info['agency_id'];//代理店ID
		if ($agency_id == "") {
			$agency_id = 0;
		}
		$chain_id = $shop_info['chain_id'];//加盟店ID
		if ($chain_id == "") {
			$chain_id = 0;
		}
		$get_theme_info = get_theme_info($shop_id, $agency_id, $chain_id);
		if($get_theme_info) {
			rencode(0, '', $get_theme_info);
		}else{
			rencode(0,'',(object)array());
		}
		break;
}
?>