<?php
switch($a)
{
	//チーム追加
	case 'add_team':
		check_parameter(array($member_id,$shop_auth_code));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$shop_auth_code= html_tag_chg(SBC_DBC(trim(urldecode($shop_auth_code)),0));//チーム認証用コード
		//check_sign($_GET,$sign);
		$login_member_info = get_member_info($member_id);
//		$login_shop_info = get_shop_info($shop_id);
		$shop_info = check_shop_auth_code($shop_auth_code);
		$shop_id=$shop_info['id'];
		if(!$shop_info)
		{
			rencode(1,'該当チームが存在しません。');
		}else{
			$login_shop_info = get_shop_info($shop_info['id']);
			$duplicate_shop_id_count=get_duplicate_shop_id_count($shop_info['id'],$member_id);
			if($duplicate_shop_id_count!="0"){
				rencode(1, 'このチームは既に追加されています。');
			}else {
				if ($login_member_info['status'] == "2" && $login_shop_info['status'] == "0") {
					//			$var = update_member_shop_info($member_id,$shop_id);
					$var = add_team_info($member_id, $shop_info['id']);
					$exec_file=DOCUMENT_ROOT.VERSION_PATH.'common/thread_push_reg_shop.php';
					exec("nohup php -c '' $exec_file $member_id $shop_id  > /dev/null &");
//					//Push通知<ユーザー名>さんがチームに参加しました。 ウェルカムThanks!を贈ろう！
//					$member_array = get_shop_member_list($shop_info['id']);
//					$reg_member_info = get_shop_member_info($member_id, $shop_info['id']);
//					if ($member_array) {
//						foreach ($member_array as $key => $target_member_info) {
//							if ($target_member_info['status'] == "2" && $target_member_info['birthday_notice_flg'] == "0") {
//								$notice_msg = $reg_member_info['member_name'] . "さんがチームに参加しました。 ウェルカムThanks!を贈ろう！ ";
//								$notice_msg = html_tag_chg(SBC_DBC(trim(urldecode($notice_msg)), 0));//Noticeメッセージ
//								if ($target_member_info['member_id'] != $member_id) {
//									#通知設定有無にかかわらずお知らせ情報は作れる
//									$notice_id = creat_notice($target_member_info['member_id'], $notice_msg, $member_id, $shop_info['id']);
//									send_push_notice($target_member_info, $notice_msg,"1",$target_member_info['birthday_notice_flg']);
//								}
//							}
//						}
//					}
					rencode(0, 'チームを追加しました。', $shop_info['id'], 'shop_id');

				} else {
					if ($login_shop_info['status'] != "0") {
						rencode(1, '現在このチームのThanks!は利用停止中です。');
					} else {
						rencode(1, 'このユーザーは退会済みです。');
					}
				}
			}
		}
		break;
	//チームログイン
	case 'active_team':
		check_parameter(array($member_id,$shop_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		//check_sign($_GET,$sign);
		$login_member_info = get_member_info($member_id);
		$login_shop_info = get_shop_info($shop_id);
//		if($login_member_info['status']=="2"&&$login_shop_info['status']=="0") {
			$var = active_shop($member_id,$shop_id);
			if ($var) {
				rencode(0, 'チームを切替しました。');
			}else {
				rencode(1, 'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
//		}else{
//			if($login_shop_info['status']!="0"){
//				rencode(1,'現在このチームのThanks!は利用停止中です。');
//			}else{
//				rencode(1,'このユーザーは退会済みです。');
//			}
//		}
		break;
	//チーム削除
	case 'delete_team':
		check_parameter(array($member_id,$shop_id));
		$member_id= intval(html_tag_chg($member_id));//日記作成者ID
		$shop_id= intval(html_tag_chg($shop_id));//チームID

		//check_sign($_GET,$sign);
		$login_member_info = get_member_info($member_id);
		$login_shop_info = get_shop_info($login_member_info['shop_id']);
//		if($login_member_info['status']=="2"&&$login_shop_info['status']=="0") {
			$var = delete_team_info($member_id,$shop_id);
			if ($var) {
				rencode(0, 'チームを削除しました。');
			}else {
				rencode(1, 'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
//		}else{
//			if($login_shop_info['status']!="0"){
//				rencode(1,'現在このチームのThanks!は利用停止中です。');
//			}else{
//				rencode(1,'このユーザーは退会済みです。');
//			}
//		}
		break;
	//チーム一覧取得
	case 'get_team_list':
		check_parameter(array($member_id,$shop_id));
		//check_sign($_GET,$sign);
		$tmp = array ();

		$list = get_team_list($member_id);

		if ($list) {
			foreach ( $list as $key => $val ) {
				$shop_info = get_shop_info($val ['shop_id']);
				if($shop_info['status']!='0'){
//					$tmp [$key] ['team_name'] =$shop_info ['aname']."(利用停止)";
					$tmp [$key] ['team_name'] = $shop_info ['aname'];
					$tmp [$key] ['active_flg'] =$val ['active_flg'];
				}else{
					$tmp [$key] ['team_name'] = $shop_info ['aname'];
					$tmp [$key] ['active_flg'] = $val ['active_flg'];
				}
				$tmp [$key] ['member_id'] = $val ['member_id'];
				$tmp [$key] ['shop_id'] = $val ['shop_id'];
				$tmp [$key] ['unread_count'] = get_unread_count($member_id,$val ['shop_id']);
			}
		}

//		$re_arr=array_merge($tmp1,$tmp);
//		array_multisort($re_arr);
		$arr['team_list']=$tmp;
		if($arr){
			rencode(0,'',$arr);
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		break;

}
?>