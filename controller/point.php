<?php
switch($a)
{
	case 'get_point_info':
		check_parameter(array($shop_id,$member_id));
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		$member_id= intval(html_tag_chg($member_id));//ユーザーID		
		$shop_info = get_shop_info($shop_id);
		//獲得ポイント、消費ポイント　トータル
		$trade_point_total = get_point_total($member_id,$shop_id);
		//月間交換リクエストポイント（失敗は除外する）
		$monthly_exchange_info=get_monthly_exchange_info($member_id,$shop_id);
		
		if($trade_point_total){
			$point_info['holding_points']=$trade_point_total['total_income_points']-$trade_point_total['total_cost_points'];
			$point_info['total_receive_points']=$trade_point_total['total_income_points']+0;
			$point_exchange_limit=$shop_info['point_exchange_limit'];
			$point_exchange_left=$point_exchange_limit-$monthly_exchange_info['monthly_exchange_point'];
			$point_info['point_exchange_status']=$shop_info['point_exchange_status'];
			$point_info['point_exchange_limit']=$point_exchange_limit;
			if($shop_info['point_exchange_limit']==0){
				$point_info['reached_exchange_limit']="0";//0：上限に達しない 1：上限に達する
				$point_info['reached_exchange_limit_msg']="毎月ポイント交換は無制限です";
			}else{
				if($point_exchange_left<=0){
					$point_info['reached_exchange_limit']="1";//0：上限に達しない 1：上限に達する
					$point_info['reached_exchange_limit_msg']="今月はすでに最大ポイント数まで交換済です。";
				}else{
					$point_info['reached_exchange_limit']="0";//0：上限に達しない 1：上限に達する
					$point_info['reached_exchange_limit_msg']="今月は".$point_exchange_left."ポイントまで交換が可能です";
				}
			}
		rencode(0,'',$point_info);
		}	
		break;

	case 'get_point_trade_list':
		check_parameter(array($member_id,$shop_id,$page));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		$page = intval($page);
		$page_number = PAGE_MAX;

		$list = get_point_trade_list($member_id,$shop_id,$page,$page_number);

		$total = get_receive_point_total_count($member_id,$shop_id);
		$l = ($total%$page_number) > 0 ? 1 : 0;
		$total_page = intval($total/$page_number) + $l;

		if($total!='0'){
			$arr['point_trade_list'] = $list;
			$arr['total_page'] = $total_page;
		}else{
			$arr['point_trade_list'] = array();
			$arr['total_page'] = '0';
		}
		rencode(0,'',$arr);
		break;

	case 'get_point_exchange_list':
		check_parameter(array($member_id,$shop_id,$page));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		$page = intval($page);
		$page_number = PAGE_MAX;

		$list = get_point_exchange_list($member_id,$shop_id,$page,$page_number);

		$total = get_point_exchange_total_count($member_id,$shop_id);
		$l = ($total%$page_number) > 0 ? 1 : 0;
		$total_page = intval($total/$page_number) + $l;

		if($total!='0'){
			$arr['point_exchange_list'] = $list;
			$arr['total_page'] = $total_page;
		}else{
			$arr['point_exchange_list'] = array();
			$arr['total_page'] = '0';
		}
		rencode(0,'',$arr);
		break;

	case 'get_point_item_list':
		check_parameter(array($page));
		$page = intval($page);
		$page_number = PAGE_MAX;

		$list = get_point_item_list($shop_id,$page,$page_number);

		$total = get_point_item_total_count();
		$l = ($total%$page_number) > 0 ? 1 : 0;
		$total_page = intval($total/$page_number) + $l;

		if($total!='0'){
			$arr['point_item_list'] = $list;
			$arr['total_page'] = $total_page;
		}else{
			$arr['point_item_list'] = array();
			$arr['total_page'] = '0';
		}
		rencode(0,'',$arr);
		break;

	case 'get_point_item_info':
		check_parameter(array($item_id,$member_id,$shop_id));
		$item_id= intval(html_tag_chg($item_id));//アイテムID
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		
		$point_item_info = get_point_item_info($item_id);
		$target_value = get_point_item_target_value($item_id,$member_id,$shop_id);
		if($point_item_info){
			//過去にT-POINTカード番号など記入があった場合はデフォルト値としてセットする
			if($target_value){
				$point_item_info['target_value']=$target_value['target_value'];
			}else{
				$point_item_info['target_value']="";

			}
			rencode(0,'',$point_item_info);
		}
		break;

	case 'send_point_exchange_info':
		check_parameter(array($member_id,$shop_id,$item_id));
		$member_id= intval(html_tag_chg($member_id));//FromユーザーID
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		$item_id= intval(html_tag_chg($item_id));//アイテムID
//		$exchange_point= intval(html_tag_chg($exchange_point));//アイテムID

		$member_info = get_shop_member_info($member_id,$shop_id);
		$shop_info = get_shop_info($shop_id);
		$point_item_info = get_point_item_info($item_id);
		//月間交換リクエストポイント（失敗は除外する）
		$monthly_exchange_info=get_monthly_exchange_info($member_id,$shop_id);
		$monthly_exchange_points=$monthly_exchange_info['monthly_exchange_point'];

		//獲得ポイント、消費ポイント　トータル
		$trade_point_total = get_point_total($member_id,$shop_id);
		$holding_points=$trade_point_total['total_income_points']-$trade_point_total['total_cost_points'];

		if($member_info['status']=="2"&&$shop_info['status']=="0"&&($monthly_exchange_points<$shop_info['point_exchange_limit']||$shop_info['point_exchange_limit']=="0")&&$holding_points>=$point_item_info['item_point']) {
			$var = send_point_exchange_info($member_id,$shop_id,$item_id,$point_item_info['item_point'],$target_value);
			$exchange_id=$var;
			if($var) {
				$var = send_point_trade_info($member_id,$shop_id,$exchange_id,$point_item_info['item_point']);
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
			$trade_id=$var;
			$var = update_point_exchange_info($exchange_id,$trade_id);
			if($var){
//				$to = 'support@aruto.me';
				$to = 'fivbak@gmail.com';
				$subject = '【thanks!】ポイント交換リクエストありました。';
				$body = <<<EOF
ユーザーID:{$member_id}
リクエストID:{$var}

EOF;
				$member_email=$member_info['email'];
				$member_name=$member_info['member_name'];
//				$header = 'From:'.mb_encode_mimeheader($member_name).'<'.$member_email.'>'."\n";
				$header = 'From:'.mb_encode_mimeheader('thanks! 運営チーム').'<support@aruto.me>'."\n";
				mb_language('uni');
				mb_internal_encoding('UTF-8');
				mb_send_mail($to, $subject, $body, $header);
				rencode(0,'お申込みが完了しました。 ');
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}elseif($holding_points<$point_item_info['item_point']){
			rencode(1,'ポイントが不足しています。');
		}elseif($monthly_exchange_info>=$shop_info['point_exchange_limit']&&$shop_info['point_exchange_limit']!="0"){
			rencode(1,'今月の交換上限を超えています。');
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		break;

	case 'get_point_exchange_result':
		check_parameter(array($shop_id,$member_id));
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		$member_id= intval(html_tag_chg($member_id));//ユーザーID

		$point_exchange_result = get_point_exchange_result($member_id,$shop_id);
		if($point_exchange_result){
			$exchange_id=$point_exchange_result['exchange_id'];
			$var = set_read_status($exchange_id);
			if($var){
				$point_exchange_result['request_datetime']=date('m月d日',strtotime($point_exchange_result['request_datetime']));
				rencode(0,'',$point_exchange_result);
			}else{
				rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
			}
		}else{
			$arr=(object)array();
			rencode(0,'',$arr);
		}
		break;

	case 'set_result_read_flg':
		check_parameter(array($exchange_id));
		$exchange_id= intval(html_tag_chg($exchange_id));//交換ID

		$var = set_read_status($exchange_id);
		if($var){
			rencode(0,'');
		}else{
			rencode(1,'エラーが発生しました。しばらく待ってから再度試してみてください。');
		}
		break;
}
?>