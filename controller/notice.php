<?php
switch($a)
{
	//獲得お知らせリスト取得
	case 'get_receive_notice_list':
		check_parameter(array($member_id,$shop_id,$page));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		$page = intval($page);
		//check_sign($_GET,$sign);
		$page_number = PAGE_MAX;

		$list = get_receive_notice_list($member_id,$shop_id,$page,$page_number);
		if ($list) {
			$var=update_all_notice_read_flg($member_id,$shop_id);
			$i=0;
			foreach ( $list as $key => $val ) {
				$from_member_info = get_shop_member_info($val ['from_id'],$val ['shop_id']);
				if($from_member_info['status']=="2"||$from_member_info['bot_flg']=="1"){
					$ret=update_notice_read_flg($val['notice_id']);
					$tmp [$i] ['notice_id'] = $val ['notice_id'];
					$tmp [$i] ['shop_id'] = $val ['shop_id'];
					$tmp [$i] ['diary_id'] = $val ['diary_id'];
					$tmp [$i] ['notice_msg'] = $val ['notice_msg'];
					$tmp [$i] ['target_id'] = $val ['target_id'];
					$tmp [$i] ['from_id'] = $val ['from_id'];
					$tmp [$i] ['send_time'] = $val ['send_time'];
					$tmp [$i] ['status'] = $val ['status'];
					$tmp [$i] ['notice_type'] = $val ['notice_type'];
					$tmp [$i] ['read_time'] = $val ['read_time'];
					$tmp [$i] ['del_flg'] = $val ['del_flg'];
					$tmp [$i] ['member_name'] = $from_member_info ['member_name'];
					$tmp [$i] ['profile_img_url'] = $from_member_info ['profile_img_url'];
					$tmp [$i] ['from_member_status'] = $val ['from_member_status'];
					$i++;
				}else{
					continue;
				}

			}
		}
		$total = get_receive_notice_total_count($member_id,$shop_id);
		$l = ($total%$page_number) > 0 ? 1 : 0;
		$total_page = intval($total/$page_number) + $l;
		$cnt = get_unread_notice_count($member_id,$shop_id);
		if($cnt!='0'){
			$arr ['unread_notice_count'] = $cnt;
		}else{
			$arr ['unread_notice_count'] = '0';
		}
		if($total!='0'){
			$arr['receive_notice_list'] = $tmp;
			$arr['total_page'] = $total_page;
		}else{
			$arr['receive_notice_list'] = array();
			$arr['total_page'] = '0';
		}
		rencode(0,'',$arr);
		break;
	//未読お知らせリスト取得（20レコードのみ）
	case 'get_receive_unread_notice_list':
		check_parameter(array($member_id,$shop_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		$shop_id= intval(html_tag_chg($shop_id));//チームID
		//check_sign($_GET,$sign);
		$tmp = array ();
		$list = get_receive_unread_notice_list($member_id,$shop_id);
		if ($list) {
			$var=update_all_notice_read_flg($member_id,$shop_id);
			$i=0;
			foreach ( $list as $key => $val ) {
				$from_member_info = get_shop_member_info($val ['from_id'],$val ['shop_id']);
				if($from_member_info['status']=="2"||$from_member_info['bot_flg']=="1"){
					$ret = update_notice_read_flg($val['notice_id']);
					$tmp [$i] ['notice_id'] = $val ['notice_id'];
					$tmp [$i] ['shop_id'] = $val ['shop_id'];
					$tmp [$i] ['diary_id'] = $val ['diary_id'];
					$tmp [$i] ['notice_msg'] = $val ['notice_msg'];
					$tmp [$i] ['target_id'] = $val ['target_id'];
					$tmp [$i] ['from_id'] = $val ['from_id'];
					$tmp [$i] ['send_time'] = $val ['send_time'];
					$tmp [$i] ['status'] = $val ['status'];
					$tmp [$i] ['notice_type'] = $val ['notice_type'];
					$tmp [$i] ['read_time'] = $val ['read_time'];
					$tmp [$i] ['del_flg'] = $val ['del_flg'];
					$tmp [$i] ['member_name'] = $from_member_info ['member_name'];
					$tmp [$i] ['profile_img_url'] = $from_member_info ['profile_img_url'];
					$tmp [$i] ['from_member_status'] = $val ['from_member_status'];
					$i++;
				}else{
					continue;
				}
			}
		}
		$cnt = get_unread_notice_count($member_id,$shop_id);
		if($cnt!='0'){
			$arr ['unread_notice_count'] = $cnt;
		}else{
			$arr ['unread_notice_count'] = '0';
		}
		if($list){
			$arr['receive_notice_list'] = $tmp;
		}else{
			$arr['receive_notice_list'] = array();
		}
		rencode(0,'',$arr);
		break;
	
	//未読お知らせ数取得
	case 'get_unread_notice_count':
		check_parameter(array($member_id));
		$member_id= intval(html_tag_chg($member_id));//ユーザーID
		//check_sign($_GET,$sign);

		$cnt = get_unread_notice_count($member_id,$shop_id);

		if($cnt!='0'){
			$arr['unread_notice_count'] = $cnt;
		}else{
			$arr['unread_notice_count'] = '0';
		}
		rencode(0,'',$arr);
		break;
}
?>