<?php
include('include/init.php');

$todays_date = date("Y-m-d");
$todays_day = date("d");
$todays_month = date("n");
$todays_month_with_zero = date("m");
$last_month_with_zero = date("m",strtotime("-1 month"));
$last_month_year = date("Y",strtotime("-1 month"));
$first_day = date('Y-m-01', strtotime($todays_date));
$end_day= date('d', strtotime("$first_day +1 month -1 day"));

$shop_list = get_all_shop_list();
if ($shop_list) {
	foreach ($shop_list as $key => $val) {
//		先月のMVP
		if (($todays_day=='01')) {
			$mvp_member_info = get_mvp_member_info($val['shop_id'], $last_month_with_zero, $last_month_year);
			$shop_member_list = get_shop_member_list($val['shop_id']);
			if ($mvp_member_info) {
				foreach ($shop_member_list as $s_key => $s_val) {
					if ($s_val['device_type'] == "1") {
						$thanks_msg = $todays_month . "月が始まりました！先月の獲得thanks!数１位 は" . $mvp_member_info['member_name'] . "さんでした！おめでとうございます！今月も皆でthanks!を贈り合いましょう！";
						#通知設定有無にかかわらずお知らせ情報は作れる
						$notice_id = creat_notice($s_val['member_id'], $thanks_msg, $mvp_member_info['member_id'], $val['shop_id']);
						error_log($thanks_msg . "\r\n", 3, DOCUMENT_ROOT.'log/gen.log');
						if ($s_val['birthday_notice_flg'] == "0" && $s_val['device_id'] != "") {
							$badge = get_unread_thanks_count($s_val['member_id'], $val['shop_id']);
	//						$var = send_push_ios($thanks_msg, $s_val['device_id'], $badge);
							$var = send_push_ios_with_type($thanks_msg, $s_val['device_id'], $badge, "1");//お知らせ一覧に遷移
						}
					} else {
						$thanks_msg = $todays_month . "月が始まりました！先月の獲得thanks!数１位 は" . $mvp_member_info['member_name'] . "さんでした！おめでとうございます！今月も皆でthanks!を贈り合いましょう！";
						error_log($thanks_msg . "\r\n", 3, DOCUMENT_ROOT.'log/gen.log');
						#通知設定有無にかかわらずお知らせ情報は作れる
						$notice_id = creat_notice($s_val['member_id'], $thanks_msg, $mvp_member_info['member_id'], $val['shop_id']);
						if ($s_val['birthday_notice_flg'] == "0" && $s_val['device_id'] != "") {
							$registatoin_ids = array($s_val['device_id']);
	//						$thanks_msg = array("msg" => $thanks_msg);
	//						$var = send_push_android($thanks_msg, $registatoin_ids);
							$var = send_push_android_with_type($thanks_msg, $registatoin_ids, "1");//お知らせ一覧に遷移
						}
					}
				}
			}
		}

	}
}

?>
