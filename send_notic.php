<?php
include('include/init.php');

$todays_date = date("Y-m-d");
$todays_day = date("d");
$todays_month = date("n");
$todays_month_with_zero = date("m");
$todays_year = date("Y");
$first_day = date('Y-m-01', strtotime($todays_date));
$end_day= date('d', strtotime("$first_day +1 month -1 day"));

$list = get_all_member_list();
if ($list) {
	foreach ( $list as $key => $val ) {
		$startdate=strtotime($val['service_start_day']);
		$enddate=strtotime(date('Y-m-d',time()));
		$days=round(($enddate-$startdate)/3600/24) ;

		$insert_time=strtotime($val['insert_time']);
		$today=strtotime(date('Y-m-d 23:59:59',time()));
		$sign_days=round(($today-$insert_time)/3600/24) ;

		$thanks_msg="";
		$target_member_name=$val['member_name'];

		$shop_info = get_shop_info($val['shop_id']);
		if($shop_info['status']=="0") {
			//メンバーの勤続記念日 （100日ごと）
			if (($days%100==0&&$days!=0&&$days>=100)) {
				$shop_member_list = get_shop_member_list($val['shop_id']);
				foreach ( $shop_member_list as $s_key => $s_val ) {
					if ($s_val['member_id'] != $val['member_id']) {
						if ($s_val['device_type'] == "1") {
							$thanks_msg = "今日は" . $target_member_name . "さん勤続" . $days . "日目です！皆でthanks!メッセージを送りましょう！";
							#通知設定有無にかかわらずお知らせ情報は作れる
							$notice_id = creat_notice($s_val['member_id'], $thanks_msg, $val['member_id'], $val['shop_id']);
							error_log($thanks_msg . "\r\n", 3, DOCUMENT_ROOT . 'log/gen.log');
							if ($s_val['birthday_notice_flg'] == "0" && $s_val['device_id'] != "") {
								$badge = get_unread_thanks_count($s_val['member_id'], $val['shop_id']);
								//							$var = send_push_ios($thanks_msg, $s_val['device_id'], $badge);
								$var = send_push_ios_with_type($thanks_msg, $s_val['device_id'], $badge, "1");//お知らせ一覧に遷移
							}
						} else {
							$thanks_msg = "今日は" . $target_member_name . "さん勤続" . $days . "日目です！皆でthanks!メッセージを送りましょう！";
							error_log($thanks_msg . "\r\n", 3, DOCUMENT_ROOT . 'log/gen.log');
							#通知設定有無にかかわらずお知らせ情報は作れる
							$notice_id = creat_notice($s_val['member_id'], $thanks_msg, $val['member_id'], $val['shop_id']);
							if ($s_val['birthday_notice_flg'] == "0" && $s_val['device_id'] != "") {
								$registatoin_ids = array($s_val['device_id']);
								//							$thanks_msg = array("msg" => $thanks_msg);
								//							$var = send_push_android($thanks_msg, $registatoin_ids);
								$var = send_push_android_with_type($thanks_msg, $registatoin_ids, "1");//お知らせ一覧に遷移
							}
						}
					}
				}
			}
			//メンバーの誕生日
			if (substr($val['birthday'], 5, 5) == date("m-d", time())) {
				$shop_birthday_member_list = get_shop_member_list($val['shop_id']);
				foreach ( $shop_birthday_member_list as $sb_key => $sb_val ) {
					if ($sb_val['member_id'] != $val['member_id']) {
						if ($sb_val['device_type'] == "1") {
							$thanks_msg = "今日は" . $target_member_name . "さんの誕生日です！皆でお祝いメッセージを送りましょう！";
							error_log($thanks_msg . "\r\n", 3, DOCUMENT_ROOT . 'log/gen.log');
							#通知設定有無にかかわらずお知らせ情報は作れる
							$notice_id = creat_notice($sb_val['member_id'], $thanks_msg, $val['member_id'], $val['shop_id']);
							if ($sb_val['birthday_notice_flg'] == "0" && $sb_val['device_id'] != "") {
								$badge = get_unread_thanks_count($sb_val['member_id'], $val['shop_id']);
//							$var = send_push_ios($thanks_msg, $sb_val['device_id'], $badge);
								$var = send_push_ios_with_type($thanks_msg, $sb_val['device_id'], $badge, "1");//お知らせ一覧に遷移
							}
						} else {
							$thanks_msg = "今日は" . $target_member_name . "さんの誕生日です！皆でお祝いメッセージを送りましょう！";
							error_log($thanks_msg . "\r\n", 3, DOCUMENT_ROOT . 'log/gen.log');
							#通知設定有無にかかわらずお知らせ情報は作れる
							$notice_id = creat_notice($sb_val['member_id'], $thanks_msg, $val['member_id'], $val['shop_id']);
							if ($sb_val['birthday_notice_flg'] == "0" && $sb_val['device_id'] != "") {
								$registatoin_ids = array($sb_val['device_id']);
//							$thanks_msg = array("msg" => $thanks_msg);
//							$var = send_push_android($thanks_msg, $registatoin_ids);
								$var = send_push_android_with_type($thanks_msg, $registatoin_ids, "1");//お知らせ一覧に遷移
							}
						}
					}
				}
			}
		}
	}
}
?>
